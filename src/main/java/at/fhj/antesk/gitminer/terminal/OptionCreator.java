package at.fhj.antesk.gitminer.terminal;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class OptionCreator {

    public static Options createOptions() {
        Options options = new Options();

        buildMandatoryOptions(options);
        buildOptionalOptions(options);

        return options;
    }

    private static void buildMandatoryOptions(Options options) {
        options.addOption(Option.builder()
                .longOpt("path")
                .desc("The path where the .git directory is located.")
                .hasArg(true)
                .argName("repositoryPath")
                .optionalArg(false)
                .required(true)
                .build());
    }

    private static void buildOptionalOptions(Options options) {
        options.addOption(Option.builder()
                .longOpt("ref")
                .desc("The git ref. If omitted, HEAD will be used.")
                .hasArg(true)
                .argName("ref")
                .optionalArg(false)
                .required(false)
                .build());

        options.addOption(Option.builder()
                .longOpt("name")
                .desc("A nice display name for the repository.")
                .hasArg(true)
                .argName("name")
                .optionalArg(false)
                .required(false)
                .build());

        options.addOption(Option.builder()
                .longOpt("url")
                .desc("The URL of the repository.")
                .hasArg(true)
                .argName("url")
                .optionalArg(false)
                .required(false)
                .build());

        options.addOption(Option.builder()
                .longOpt("testMode")
                .desc("Only for testing. No changes will be persisted.")
                .required(false)
                .build());
    }


}
