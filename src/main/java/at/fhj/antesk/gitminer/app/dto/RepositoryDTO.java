package at.fhj.antesk.gitminer.app.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.eclipse.jgit.lib.Constants;

public class RepositoryDTO {

    private String name;
    private String url;
    private String path;
    private String ref;

    public RepositoryDTO(String name, String url, String path, String ref) {
        this.name = name;
        this.url = url;
        this.path = path;
        this.ref = ref;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRef() {
        if (ref == null) {
            return Constants.HEAD;
        }

        return ref;
    }

    public void setRef(String ref) {
        if (ref == null) {
            ref = Constants.HEAD;
        }

        this.ref = ref;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        RepositoryDTO that = (RepositoryDTO) o;

        return new EqualsBuilder()
                .append(name, that.name)
                .append(url, that.url)
                .append(path, that.path)
                .append(ref, that.ref)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(url)
                .append(path)
                .append(ref)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("url", url)
                .append("path", path)
                .append("ref", ref)
                .toString();
    }
}
