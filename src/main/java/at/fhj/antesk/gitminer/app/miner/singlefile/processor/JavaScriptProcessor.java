package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.AbstractProcessor;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaScriptProcessor extends AbstractProcessor {

    final static Pattern BASIC_COMMENT_PATTERN = Pattern
            .compile("(\\/\\*[\\w\\'\\s\\r\\n\\*]*\\*\\/)|(\\/\\/[\\w\\s\\']*)|(\\<![\\-\\-\\s\\w\\>\\/]*\\>)");

    public JavaScriptProcessor(SourceFileResults sourceFileResults) {
        super(sourceFileResults);
    }

    @Override
    public void process() {
        super.process();

        SourceFile sourceFile = sourceFileResults.getSourceFile();

        String fileContents = sourceFile.getFileContents();

        Matcher matcher = BASIC_COMMENT_PATTERN.matcher(fileContents);
        while (matcher.find()) {
            sourceFileResults.addComment(matcher.group(1));
            sourceFileResults.addComment(matcher.group(2));
            sourceFileResults.addComment(matcher.group(3));
        }
    }

}
