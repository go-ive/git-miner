package at.fhj.antesk.gitminer.app.miner.singlefile.processor.common;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.utils.SourceFileUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractProcessor implements Processor {

    private static Logger logger = Logger.getLogger(AbstractProcessor.class);

    protected SourceFileResults sourceFileResults;

    private static final int DEFAULT_VERSION = 1;

    private static final Pattern EMAIL_PATTERN = Pattern.compile("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,8}");
    private static final Pattern AWS_CLIENT_ID_PATTERN = Pattern.compile("AKIA[0-9A-Z]{16}");
//    private static final Pattern AWS_SECRET_KEY_PATTERN = Pattern.compile("[0-9a-zA-Z/+]{40}");

    public AbstractProcessor(SourceFileResults sourceFileResults) {
        sourceFileResults.setVersion(DEFAULT_VERSION);

        this.sourceFileResults = sourceFileResults;
    }

    @Override
    public void process() {
        logger.trace("Processing " + sourceFileResults.getSourceFile().getName());

        String fileContents = sourceFileResults.getSourceFile().getFileContents();

        parseValue(EMAIL_PATTERN, fileContents, GitMinerConstants.PK_COMMON_EMAIL_ADDRESSES);
        parseUrls(sourceFileResults.getSourceFile());

        parseValue(AWS_CLIENT_ID_PATTERN, fileContents, GitMinerConstants.PK_AWS_APIKEY_CLIENT_IDS);
//        parseValue(AWS_SECRET_KEY_PATTERN, fileContents, GitMinerConstants.PK_AWS_APIKEY_SECRET_KEYS);
    }

    private void parseUrls(SourceFile sourceFile) {
        String[] parts = sourceFile.getFileContents().split("\\s+");

        if (parts.length < 1) {
            return;
        }

        Arrays.asList(parts)
                .stream()
                .map(SourceFileUtils::parseAbsoluteUrl)
                .filter(string -> string != null)
                .forEach(string -> sourceFileResults.addToList(GitMinerConstants.PK_COMMON_URLS, string));
    }

    private void parseValue(Pattern pattern, String fileContents, String key) {
        Matcher matcher = pattern.matcher(fileContents);

        while (matcher.find()) {
            sourceFileResults.addToList(key, matcher.group());
        }
    }
}
