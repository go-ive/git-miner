package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.AbstractProcessor;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.xml.AndroidManifestParser;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.xml.PomParser;
import at.fhj.antesk.gitminer.app.utils.FileUtils;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlProcessor extends AbstractProcessor {

    private static Logger logger = Logger.getLogger(XmlProcessor.class);

    private static final String ANDROID_MANIFEST_XML = "AndroidManifest.xml";
    private static final String POM_XML = "pom.xml";
    private static final String COMMENT_PATTERN = "\\<\\!\\-\\-(.*)\\-\\-";

    public XmlProcessor(SourceFileResults sourceFileResults) {
        super(sourceFileResults);
    }

    @Override
    public void process() {
        super.process();

        SourceFile sourceFile = sourceFileResults.getSourceFile();

        logger.trace("Running XmlProcessor for file " + sourceFile.getName());

        parseCommentsUsingRegex(sourceFile);

        String fileName = FileUtils.extractFileName(sourceFile.getName());

        if (ANDROID_MANIFEST_XML.equalsIgnoreCase(fileName)) {
            AndroidManifestParser androidManifestParser = new AndroidManifestParser(sourceFileResults);
            androidManifestParser.parse();
        }

        if (POM_XML.equalsIgnoreCase(fileName)) {
            PomParser pomParser = new PomParser(sourceFileResults);
            pomParser.parse();
        }
    }

    private void parseCommentsUsingRegex(SourceFile sourceFile) {
        Pattern pattern = Pattern.compile(COMMENT_PATTERN);
        Matcher matcher = pattern.matcher(sourceFile.getFileContents());
        while (matcher.find()) {
            sourceFileResults.addComment(matcher.group(1));
        }
    }

}
