package at.fhj.antesk.gitminer.app.config;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

    private static Config INSTANCE;
    private static Logger logger = Logger.getLogger(Config.class);

    private Properties properties;

    private Config() throws IOException {
        properties = new Properties();
        properties.load(new FileInputStream(new File("config.properties")));

        initDefaultRuntimeValues();
    }

    private void initDefaultRuntimeValues() {
        if (properties.getProperty(KEYS.GM_HOME_PATH.getKey()) == null) {
            properties.put(KEYS.GM_HOME_PATH.getKey(), System.getProperty("user.home") + "/.gitminer");
        }
    }

    public static Config getInstance() {
        if (INSTANCE == null) {
            try {
                INSTANCE = new Config();
            } catch (IOException e) {
                logger.error(e.getMessage());
                logger.debug("Could not load config.properties file", e);
                System.exit(1);
            }
        }

        return INSTANCE;
    }

    public enum KEYS {
        POSTGRES_DB_URL("postgresDbUrl"),
        POSTGRES_DB_USER("postgresDbUser"),
        POSTGRES_DB_PASSWORD("postgresDbPass"),

        H2_DB_URL("h2DbUrl"),
        H2_DB_USER("h2DbUser"),
        H2_DB_PASSWORD("h2DbPass"),

        CLI_PARAM_REPOSITORY_PATH("cliParamPath"),
        ANALYSIS_MAX_THREADS("maxAnalysisThreads"),
        GM_HOME_PATH("gitMinerHomePath"),
        CLEANUP_AFTER_ANALYSIS("cleanupAfterAnalysis");

        private String key;

        public String getKey() {
            return key;
        }

        KEYS(String key) {
            this.key = key;
        }
    }

    public String getProperty(KEYS key) {
        String property = properties.getProperty(key.getKey());

        if (property == null) {
            logger.debug("Property for key " + key.getKey() + " not found!");
        }

        return property;
    }

    public void setProperty(KEYS key, Object value) {
        properties.put(key.getKey(), value);
    }

}
