package at.fhj.antesk.gitminer.app.miner.singlefile.processor.common;

/**
 * Created by ive on 12/4/15.
 */
public interface Processor {

    void process();

}
