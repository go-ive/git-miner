package at.fhj.antesk.gitminer.app.analyzer;

public interface Analyzer {

    void analyze();

}