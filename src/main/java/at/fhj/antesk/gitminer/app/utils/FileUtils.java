package at.fhj.antesk.gitminer.app.utils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FileUtils {

    public static String extractFileName(String path) {
        String result = path;

        int lastSlash = path.lastIndexOf("/");
        if (lastSlash != -1) {
            result = path.substring(lastSlash + 1);
        }

        return result;
    }

    public static String extractPathForFullPath(String path) {
        String result = "";

        int lastSlash = path.lastIndexOf("/");
        if (lastSlash != -1) {
            result = path.substring(0, lastSlash + 1);
        }

        return result;
    }

    public static Map<String, Integer> countFilesPerPath(List<String> paths) {
        if (paths == null) {
            throw new IllegalArgumentException("Path list must not be null");
        }

        Map<String, Integer> result = new LinkedHashMap<>();

        for (String path : paths) {
            String extractedPath = extractPathForFullPath(path);
            Integer previousAmount = result.get(extractedPath);
            if (previousAmount != null) {
                result.put(extractedPath, ++previousAmount);
            } else {
                result.put(extractedPath, 1);
            }
        }

        return result;
    }

    public static String extractFileExtension(String path) {
        String result = "";

        int lastDot = path.lastIndexOf(".");
        if (lastDot != -1) {
            result = path.substring(lastDot + 1);
        }

        return result;
    }

}
