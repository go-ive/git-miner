package at.fhj.antesk.gitminer.app;

import at.fhj.antesk.gitminer.app.analyzer.Analyzer;
import at.fhj.antesk.gitminer.app.analyzer.PostAnalyzer;
import at.fhj.antesk.gitminer.app.analyzer.PreAnalyzer;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.gitapi.GitAPI;
import at.fhj.antesk.gitminer.app.gitapi.GitAPIJGitImpl;
import at.fhj.antesk.gitminer.app.output.OutputWriter;
import at.fhj.antesk.gitminer.app.output.ReportToJsonFileWriter;
import at.fhj.antesk.gitminer.persistence.Database;
import at.fhj.antesk.gitminer.persistence.H2Database;
import at.fhj.antesk.gitminer.persistence.PostgresDatabase;

import java.io.IOException;

public class ApplicationFacade {

    private Analyzer preAnalyzer;
    private PostAnalyzer postAnalyzer;
    private GitAPI gitAPI;
    private OutputWriter outputWriter;

    public ApplicationFacade(RepositoryDTO repositoryDTO, boolean testMode) throws IOException {
        gitAPI = new GitAPIJGitImpl(repositoryDTO.getPath());

        Database database;

        if (testMode) {
            database = new H2Database();
        } else {
            database = new PostgresDatabase();
        }

        preAnalyzer = new PreAnalyzer(repositoryDTO, database, gitAPI);
        postAnalyzer = new PostAnalyzer(repositoryDTO, database);
    }

    public void run() {
        preAnalyzer.analyze();
        postAnalyzer.analyze();
        outputWriter = new ReportToJsonFileWriter(postAnalyzer.getReport());
        outputWriter.write();
    }

}
