package at.fhj.antesk.gitminer.app.dto;

import java.util.HashMap;
import java.util.Map;

public class Report {

    private transient int repositoryId = -1;
    private Map<String, Object> data = new HashMap<>();

    public int getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(int repositoryId) {
        this.repositoryId = repositoryId;
    }

    public Map<String, Object> getData() {
        return data;
    }

}
