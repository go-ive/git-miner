package at.fhj.antesk.gitminer.app.miner.singlefile.processor.xml;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class AndroidManifestParser {

    private static Logger logger = Logger.getLogger(AndroidManifestParser.class);

    private SourceFileResults sourceFileResults;

    public AndroidManifestParser(SourceFileResults sourceFileResults) {
        this.sourceFileResults = sourceFileResults;
    }

    public void parse() {
        logger.debug("Processing " + sourceFileResults.getSourceFile().getName());

        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new StringReader(sourceFileResults.getSourceFile().getFileContents())));

            parsePermissions(document);
        } catch (SAXException e) {
            logger.error(e.getMessage());
            logger.debug("Could not parse AndroidManifest.xml", e);
        } catch (IOException | ParserConfigurationException e) {
            logger.error(e.getMessage());
            logger.debug("Error in AndroidManifestParser", e);
        }
    }

    private void parsePermissions(Document document) {
        List<String> usedPermissions = new ArrayList<>();

        NodeList usesPermissionTags = document.getElementsByTagName("uses-permission");
        if (usesPermissionTags.getLength() > 0) {
            for (int i = 0; i < usesPermissionTags.getLength(); i++) {
                Node tag = usesPermissionTags.item(i);
                NamedNodeMap attributes = tag.getAttributes();
                if (attributes.getLength() > 0) {
                    for (int j = 0; j < attributes.getLength(); j++) {
                        if (attributes.item(j).getNodeName().equals("android:name")) {
                            usedPermissions.add(attributes.item(j).getNodeValue());
                            break;
                        }
                    }
                }
            }
        }

        sourceFileResults.getProperties().put(GitMinerConstants.PK_XML_ANDROID_USED_PERMISSIONS, usedPermissions);
    }

}
