package at.fhj.antesk.gitminer.app.utils;

import com.google.gson.Gson;

public class JsonUtils {

    private static JsonUtils INSTANCE;

    public final Gson gson;

    private JsonUtils() {
        this.gson = new Gson();
    }

    public static JsonUtils getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new JsonUtils();
        }

        return INSTANCE;
    }

}
