package at.fhj.antesk.gitminer.app.analyzer;

import at.fhj.antesk.gitminer.app.config.Config;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.exception.GitMinerException;
import at.fhj.antesk.gitminer.app.gitapi.GitAPI;
import at.fhj.antesk.gitminer.app.gitapi.GitAPIJGitImpl;
import at.fhj.antesk.gitminer.app.miner.Miner;
import at.fhj.antesk.gitminer.app.miner.SingleFileMiner;
import at.fhj.antesk.gitminer.app.utils.ListUtils;
import at.fhj.antesk.gitminer.persistence.Database;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class ExecutorServiceBasedPreAnalyzer implements Analyzer {

    private static Logger logger = Logger.getLogger(PreAnalyzer.class);

    private final int MAX_THREADS = Integer.valueOf(Config.getInstance().getProperty(Config.KEYS.ANALYSIS_MAX_THREADS));

    private Database database;
    private Miner<SourceFileResults, SourceFile> miner = new SingleFileMiner();
    private RepositoryDTO repositoryDTO;
    private GitAPI gitAPI;

    public ExecutorServiceBasedPreAnalyzer(RepositoryDTO repositoryDTO, Database database, GitAPI gitAPI) {
        this.repositoryDTO = repositoryDTO;
        this.database = database;
        this.gitAPI = gitAPI;
    }

    @Override
    public void analyze() {
        ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREADS);

        try {
            database.init();

            List<String> revisionHashesForRef = gitAPI.getRevisionHashesForRef(repositoryDTO.getRef());
            Collections.reverse(revisionHashesForRef);

            final int repositoryId = database.insertRepository(repositoryDTO);

            List<Runner> runners = ListUtils.splitListIntoSubLists(revisionHashesForRef, MAX_THREADS)
                    .stream()
                    .map(partialList -> new Runner(database, revisionHashesForRef, partialList, repositoryId))
                    .collect(Collectors.toList());

            executorService.invokeAll(runners);


        } catch (GitMinerException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown();
            database.close();
        }
    }

    private class Runner implements Callable<Void> {

        private Database database;
        private List<String> allRevisionHashes;
        private List<String> revisionHashesToProcess;
        private int repositoryId;

        private Runner(Database database, List<String> allRevisionHashes, List<String> revisionHashesToProcess, int repositoryId) {
            this.database = database;
            this.allRevisionHashes = allRevisionHashes;
            this.revisionHashesToProcess = revisionHashesToProcess;
            this.repositoryId = repositoryId;
        }

//        @Override
//        public void run() {
//
//        }

        private GitAPI createTemporaryGitRepository() {
            GitAPI gitAPI = null;

            try {
                // copy repository to random generated folder
                File temporaryNewDestination = new File(Config.getInstance().getProperty(Config.KEYS.GM_HOME_PATH) + "/" + RandomStringUtils.randomAlphabetic(10));
                File originalRepository = new File(repositoryDTO.getPath());

                FileUtils.forceMkdir(temporaryNewDestination);
                FileUtils.copyDirectory(originalRepository, temporaryNewDestination);

                gitAPI = new GitAPIJGitImpl(temporaryNewDestination.getAbsolutePath());
                gitAPI.reset();
            } catch (GitMinerException | IOException e) {
                logger.error(e.getMessage());
                logger.debug("Could not copy git repository to temporary directory", e);
                System.exit(1);
            }

            return gitAPI;
        }

        private boolean checkoutRevision(GitAPI internalGitApi, String currentHash) {
            try {
                internalGitApi.checkoutRevision(currentHash, true);
            } catch (GitMinerException e) {
                logger.error(e.getMessage());
                logger.debug("Error occurred for hash " + currentHash + ". Skipping...", e);
                return false;
            }

            return true;
        }

        private void processSourceFile(int repositoryId, SourceFile sourceFile) {
            SourceFileResults sourceFileResults = miner.mine(sourceFile);

            if (!sourceFileResults.getProperties().keySet().isEmpty()) {
                database.insertSourceFileResults(sourceFileResults, repositoryId);
            } else {
                logger.debug("No analysis results for " + sourceFile + ". Not persisting.");
            }
        }

        private void checkoutMasterAndDeleteLocalBranches(List<String> revisionList, GitAPI internalGitApi) {
            try {
                internalGitApi.checkoutRevision("master", false);
                internalGitApi.deleteBranch(revisionList.toArray(new String[0]));
            } catch (GitMinerException e) {
                logger.error(e.getMessage());
                logger.debug("Error occurred.", e);
            }
        }

        @Override
        public Void call() throws Exception {
            GitAPI internalGitApi = createTemporaryGitRepository();

            revisionHashesToProcess
                    .stream()
                    .forEach(currentHash -> {
                        logger.info("Processing revision " + currentHash);

                        if (checkoutRevision(internalGitApi, currentHash)) {
                            try {
                                int indexInMainList = allRevisionHashes.indexOf(currentHash);

                                List<SourceFile> sourceFiles = (indexInMainList == 0) // is first revision
                                        ? internalGitApi.fetchChangedFilesForCurrentRevision(null, currentHash)
                                        : internalGitApi.fetchChangedFilesForCurrentRevision(allRevisionHashes.get(indexInMainList - 1), currentHash);

                                sourceFiles.forEach(sourceFile -> processSourceFile(repositoryId, sourceFile));
                            } catch (GitMinerException e) {
                                logger.error(e.getMessage());
                                logger.debug("Could not retrieve list of changed files for revisions", e);
                            }

                            logger.info("Done processing revision " + currentHash);

                        }
                    });

            checkoutMasterAndDeleteLocalBranches(revisionHashesToProcess, internalGitApi);
            return null;
        }
    }

}
