package at.fhj.antesk.gitminer.app.exception;

public class GitMinerException extends Exception {

    public GitMinerException(String message) {
        super(message);
    }

    public GitMinerException(String message, Throwable cause) {
        super(message, cause);
    }

    public GitMinerException(Throwable cause) {
        super(cause);
    }
}
