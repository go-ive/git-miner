package at.fhj.antesk.gitminer.app.gitapi;

import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.exception.GitMinerException;

import java.util.List;

public interface GitAPI {

    List<String> getRevisionHashesForRef(String ref) throws GitMinerException;

    void checkoutRevision(String revision, boolean createBranch) throws GitMinerException;

    void deleteBranch(String... revisions) throws GitMinerException;

    void reset() throws GitMinerException;

    List<String> getAllRefs();

    List<SourceFile> fetchFilesForRevision(String revision);

    String getCommitMessage(String revision);

    /**
     * Retrieves a list of files that changes from the previous to the current revision.
     * <p>
     * The files returned are from the current revision.
     * </p>
     *
     * @param previous The revision that the current one should be compared to
     * @param current  The revision of which the files will be taken
     * @return
     */
    List<SourceFile> fetchChangedFilesForCurrentRevision(String previous, String current) throws GitMinerException;

}
