package at.fhj.antesk.gitminer.app.exception;

/**
 * Created by ive on 12/4/15.
 */
public class NoSuchProcessorException extends Exception {

    public NoSuchProcessorException(String message) {
        super(message);
    }

}
