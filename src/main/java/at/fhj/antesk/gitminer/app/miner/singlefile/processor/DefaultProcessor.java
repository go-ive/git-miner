package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.AbstractProcessor;

public class DefaultProcessor extends AbstractProcessor {

    public DefaultProcessor(SourceFileResults sourceFileResults) {
        super(sourceFileResults);
    }

    @Override
    public void process() {
        super.process();
    }
}
