package at.fhj.antesk.gitminer.app.gitapi;

import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.exception.GitMinerException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class GitAPIJGitImpl implements GitAPI {

    private static Logger logger = Logger.getLogger(GitAPIJGitImpl.class);

    private final Predicate<DiffEntry> modifyPredicate = p -> p.getChangeType().equals(DiffEntry.ChangeType.MODIFY);
    private final Predicate<DiffEntry> addPredicate = p -> p.getChangeType().equals(DiffEntry.ChangeType.ADD);
    private final Predicate<DiffEntry> fullPredicate = modifyPredicate.or(addPredicate);

    private Git git;
    private String repositoryPath;

    public GitAPIJGitImpl(String repositoryPath) {
        this.repositoryPath = repositoryPath;
        try {
            git = Git.open(new File(repositoryPath));
        } catch (IOException e) {
            logger.error(e.getMessage());
            logger.debug("Could not open git repository in " + repositoryPath);
        }
    }

    @Override
    public List<String> getRevisionHashesForRef(String ref) throws GitMinerException {
        List<String> result = new ArrayList<>();

        if (ref == null) {
            ref = Constants.HEAD;
        }

        try {
            git.log()
                    .add(git.getRepository().resolve(ref))
                    .call()
                    .forEach(revCommit -> result.add(revCommit.getName()));
        } catch (GitAPIException | IOException e) {
            throw new GitMinerException("Ref does not exist or is not readable.", e);
        }

        return result;
    }

    @Override
    public void checkoutRevision(String revision, boolean createBranch) throws GitMinerException {
        try {
            git.checkout()
                    .setCreateBranch(createBranch)
                    .setName(revision)
                    .setStartPoint(revision).call();
        } catch (GitAPIException e) {
            throw new GitMinerException("Error checking out branch " + revision, e);
        }
    }

    @Override
    public void deleteBranch(String... revisions) throws GitMinerException {
        try {
            git.branchDelete()
                    .setForce(true)
                    .setBranchNames(revisions)
                    .call();
        } catch (GitAPIException e) {
            throw new GitMinerException("Error deleting branches " + StringUtils.join(revisions), e);
        }
    }

    @Override
    public void reset() throws GitMinerException {
        try {
            git.reset()
                    .setMode(ResetCommand.ResetType.HARD)
                    .call();
        } catch (GitAPIException e) {
            throw new GitMinerException("Error resetting repository", e);
        }
    }

    @Override
    public List<String> getAllRefs() {
        List<String> result = new ArrayList<>();

        result.addAll(git.getRepository().getAllRefs().keySet());

        return result;
    }

    @Override
    public List<SourceFile> fetchFilesForRevision(String revision) {
        List<SourceFile> result = new ArrayList<>();

        try {
            Repository repository = git.getRepository();
            RevWalk revWalk = new RevWalk(repository);
            RevCommit commit = revWalk.parseCommit(repository.resolve(revision));

            TreeWalk treeWalk = new TreeWalk(repository);
            treeWalk.addTree(commit.getTree());
            treeWalk.setRecursive(true);

            while (treeWalk.next()) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                try {
                    repository.open(treeWalk.getObjectId(0)).copyTo(out);

                    SourceFile sourceFile = new SourceFile();
                    sourceFile.setName(treeWalk.getPathString());
                    sourceFile.setFileContents(new String(out.toByteArray()));
                    sourceFile.setRevision(revision);
                    sourceFile.setObjectId(treeWalk.getObjectId(0));
                    sourceFile.setPathOnSystem(repositoryPath + "/" + sourceFile.getName());

                    result.add(sourceFile);
                } catch (MissingObjectException moe) {
                    logger.error(moe.getMessage());
                    logger.debug("Object missing in git repository.", moe);
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            logger.debug("fetchFilesForRevision " + revision, e);
        }

        return result;
    }

    @Override
    public String getCommitMessage(String revision) {
        try {
            Repository repository = git.getRepository();
            RevWalk revWalk = new RevWalk(repository);
            RevCommit commit = revWalk.parseCommit(repository.resolve(revision));

            return commit.getFullMessage();
        } catch (IOException e) {
            logger.error(e.getMessage());
            logger.debug("getCommitMessage " + revision, e);
            return "";
        }
    }

    @Override
    public List<SourceFile> fetchChangedFilesForCurrentRevision(String previous, String current) throws GitMinerException {
        List<SourceFile> sourceFilesForCurrentRevision = fetchFilesForRevision(current);

        if (previous == null) {
            return sourceFilesForCurrentRevision;
        }

        List<SourceFile> result = new ArrayList<>();

        try {
            List<String> modifiedFiles = new ArrayList<>();

            git.diff()
                    .setOldTree(prepareTreeParser(previous))
                    .setNewTree(prepareTreeParser(current))
                    .call()
                    .stream()
                    .filter(fullPredicate)
                    .forEach(diffEntry -> {
                        if (diffEntry.getChangeType().equals(DiffEntry.ChangeType.ADD)) {
                            logger.trace("ADD change type detected for " + diffEntry.getNewPath());
                            modifiedFiles.add(diffEntry.getNewPath());
                        } else {
                            logger.trace("MODIFY change type detected for " + diffEntry.getNewPath());
                            modifiedFiles.add(diffEntry.getOldPath());
                        }
                    });

            result.addAll(sourceFilesForCurrentRevision
                    .stream()
                    .filter(sourceFile -> modifiedFiles.contains(sourceFile.getName()))
                    .collect(Collectors.toList()));

        } catch (GitAPIException | IOException e) {
            throw new GitMinerException("Could not fetch files from " + previous + " and " + current, e);
        }

        return result;
    }

    /**
     * https://github.com/centic9/jgit-cookbook/blob/master/src/main/java/org/dstadler/jgit/porcelain/ShowFileDiff.java
     */
    private AbstractTreeIterator prepareTreeParser(String objectId) throws IOException {
        Repository repository = git.getRepository();

        try (RevWalk walk = new RevWalk(repository)) {
            RevCommit commit = walk.parseCommit(ObjectId.fromString(objectId));
            RevTree tree = walk.parseTree(commit.getTree().getId());

            CanonicalTreeParser oldTreeParser = new CanonicalTreeParser();
            try (ObjectReader oldReader = repository.newObjectReader()) {
                oldTreeParser.reset(oldReader, tree.getId());
            }

            walk.dispose();

            return oldTreeParser;
        }
    }
}
