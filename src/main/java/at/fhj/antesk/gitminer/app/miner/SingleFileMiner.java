package at.fhj.antesk.gitminer.app.miner;

import at.fhj.antesk.gitminer.app.LanguageDetector;
import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.DefaultProcessor;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.Processor;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;

public class SingleFileMiner implements Miner<SourceFileResults, SourceFile> {

    private static Logger logger = Logger.getLogger(Miner.class);

    private static final String PACKAGE_PREFIX = "at.fhj.antesk.gitminer.app.miner.singlefile.processor.";
    private static final String PROCESSOR_SUFFIX = "Processor";

    public SourceFileResults mine(SourceFile sourceFile) {
        SourceFileResults sourceFileResults = new SourceFileResults();
        sourceFileResults.setSourceFile(sourceFile);

        String language = LanguageDetector.detect(sourceFileResults.getSourceFile());
        sourceFileResults.getProperties().put(GitMinerConstants.PK_COMMON_FILE_TYPE, language);

        if (language.equals(GitMinerConstants.IGNORED_FILE_TYPE)) {
            logger.debug("Not processing " + sourceFile.getName() + " because it is an ignored file type.");
            return sourceFileResults;
        }

        Processor processor = getProcessor(sourceFileResults, language);
        processor.process();

        return sourceFileResults;
    }

    public Processor getProcessor(SourceFileResults sourceFileResults, String language) {
        if (!language.equals(GitMinerConstants.UNKNOWN_FILE_TYPE)) {
            try {
                return (Processor) Class.forName(PACKAGE_PREFIX + language + PROCESSOR_SUFFIX)
                        .getConstructor(SourceFileResults.class).newInstance(sourceFileResults);
            } catch (ClassNotFoundException e) {
                logger.debug("No processor found for language " + language);
            } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException | InstantiationException e) {
                logger.error(e.getMessage());
                logger.debug("Error instantiating class for language " + language, e);
            }
        }

        return new DefaultProcessor(sourceFileResults);
    }

}
