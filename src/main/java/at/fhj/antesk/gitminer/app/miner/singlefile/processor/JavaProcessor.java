package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.AbstractProcessor;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.java.ASTUtils;
import org.apache.log4j.Logger;

public class JavaProcessor extends AbstractProcessor {

    private static Logger logger = Logger.getLogger(JavaProcessor.class);

    public JavaProcessor(SourceFileResults sourceFileResults) {
        super(sourceFileResults);
    }

    @Override
    public void process() {
        super.process();

        SourceFile sourceFile = sourceFileResults.getSourceFile();

        logger.trace("Running JavaProcessor for file " + sourceFile.getName());

        ASTUtils.parse(sourceFile.getPathOnSystem(), sourceFileResults);

    }

}
