package at.fhj.antesk.gitminer.app.dto;

import org.eclipse.jgit.lib.ObjectId;

public class SourceFile extends AbstractSourceFile {

    private String fileContents;
    private ObjectId objectId;
    private String pathOnSystem;

    public String getFileContents() {
        return fileContents;
    }

    public void setFileContents(String fileContents) {
        this.fileContents = fileContents;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public String getPathOnSystem() {
        return pathOnSystem;
    }

    public void setPathOnSystem(String pathOnSystem) {
        this.pathOnSystem = pathOnSystem;
    }

}
