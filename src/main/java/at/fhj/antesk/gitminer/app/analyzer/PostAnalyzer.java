package at.fhj.antesk.gitminer.app.analyzer;

import at.fhj.antesk.gitminer.app.analyzer.post.PostAnalyzerCallback;
import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.AbstractSourceFile;
import at.fhj.antesk.gitminer.app.dto.ModifierCountSourceFile;
import at.fhj.antesk.gitminer.app.dto.Report;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.persistence.Database;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PostAnalyzer implements Analyzer {

    private static Logger logger = Logger.getLogger(PostAnalyzer.class);

    private RepositoryDTO repositoryDTO;
    private Database database;

    private Report report;

    public PostAnalyzer(RepositoryDTO repositoryDTO, Database database) {
        this.repositoryDTO = repositoryDTO;
        this.database = database;
    }

    public Report getReport() {
        return report;
    }

    @Override
    public void analyze() {
        logger.info("Running post-analysis...");
        report = new Report();

        try {
            database.init();
            report.setRepositoryId(database.insertRepository(repositoryDTO));

            PostAnalyzerCallback postAnalyzerCallback = new PostAnalyzerCallback(report);
            database.processResults(repositoryDTO, 1, postAnalyzerCallback);

            report.getData().put(GitMinerConstants.PK_COMMON_FILES_CHANGED_IN_REVISION,
                    postAnalyzerCallback.getFileChangesInRevisions());

            fillModifierCountForJavaFiles(postAnalyzerCallback.getModifierCountSourceFiles());
        } finally {
            database.close();
        }
    }

    private void fillModifierCountForJavaFiles(List<ModifierCountSourceFile> list) {
        final Map<String, Double> publicFactorForRevisionMap = new HashMap<>();

        list.stream()
                .map(AbstractSourceFile::getRevision)
                .collect(Collectors.toSet())
                .forEach(revision -> {
                    List<ModifierCountSourceFile> filesInRevision = list.stream()
                            .filter(sourceFile -> revision.equals(sourceFile.getRevision()))
                            .collect(Collectors.toList());

                    int size = filesInRevision.size();
                    double totalPublicCount = 0;
                    for (ModifierCountSourceFile file : filesInRevision) {
                        totalPublicCount += file.getPublicFactor();
                    }

                    double revisionPublicFactor = totalPublicCount / size;

                    publicFactorForRevisionMap.put(revision, revisionPublicFactor);
                });

        report.getData().put(GitMinerConstants.PK_JAVA_PUBLIC_FACTORS, publicFactorForRevisionMap);
    }

}
