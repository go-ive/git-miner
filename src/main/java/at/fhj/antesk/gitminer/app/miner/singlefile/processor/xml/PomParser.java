package at.fhj.antesk.gitminer.app.miner.singlefile.processor.xml;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class PomParser {

    private static Logger logger = Logger.getLogger(PomParser.class);

    private SourceFileResults sourceFileResults;

    public PomParser(SourceFileResults sourceFileResults) {
        this.sourceFileResults = sourceFileResults;
    }

    public void parse() {
        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new StringReader(sourceFileResults.getSourceFile().getFileContents())));

            parseDependencies(document);
        } catch (SAXException e) {
            logger.error(e.getMessage());
            logger.debug("Could not parse pom.xml", e);
        } catch (IOException | ParserConfigurationException e) {
            logger.error(e.getMessage());
            logger.debug("Error in PomParser", e);
        }
    }

    private void parseDependencies(Document document) {
        List<String> result = new ArrayList<>();

        NodeList dependencies = document.getElementsByTagName("dependency");
        if (dependencies.getLength() > 0) {
            for (int i = 0; i < dependencies.getLength(); i++) {
                String groupId = null;
                String artifactId = null;
                String version = null;

                Node dependencyNode = dependencies.item(i);
                NodeList childNodes = dependencyNode.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node currentChild = childNodes.item(j);

                    if ("groupId".equals(currentChild.getNodeName())) {
                        groupId = currentChild.getTextContent();
                    }

                    if ("artifactId".equals(currentChild.getNodeName())) {
                        artifactId = currentChild.getTextContent();
                    }

                    if ("version".equals(currentChild.getNodeName())) {
                        version = currentChild.getTextContent();
                    }
                }

                result.add(groupId + ":" + artifactId + ":" + version);
            }
        }

        sourceFileResults.getProperties().put(GitMinerConstants.PK_XML_POM_DEPENDENCIES, result);
    }

}
