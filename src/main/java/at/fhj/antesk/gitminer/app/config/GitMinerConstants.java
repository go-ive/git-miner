package at.fhj.antesk.gitminer.app.config;

public abstract class GitMinerConstants {

    // file types
    public static final String UNKNOWN_FILE_TYPE = "Unknown";
    public static final String IGNORED_FILE_TYPE = "IngoredFileType";

    // entity types
    public static final String ENTITY_TYPE_INTERFACE = "interface";
    public static final String ENTITY_TYPE_CLASS = "class";
    public static final String ENTITY_TYPE_ENUM = "enum";

    // -----------------------------------------------------------------------------------------------------------------
    // property keys
    // -----------------------------------------------------------------------------------------------------------------

    // common
    public static final String PK_COMMON_COMMENTS = "comments";
    public static final String PK_COMMON_EMAIL_ADDRESSES = "emailAddresses";
    public static final String PK_COMMON_URLS = "urls";
    public static final String PK_COMMON_FILE_TYPE = "fileType";
    public static final String PK_COMMON_FILES_CHANGED_IN_REVISION = "filesChangedInRevision";

    // java
    public static final String PK_JAVA_PACKAGES = "package";
    public static final String PK_JAVA_ENTITY_TYPE = "entityType";
    public static final String PK_JAVA_PUBLIC_MODIFIER_COUNT = "javaPublicModifierCount";
    public static final String PK_JAVA_PRIVATE_MODIFIER_COUNT = "javaPrivateModifierCount";
    public static final String PK_JAVA_PROTECTED_MODIFIER_COUNT = "javaProtectedModifierCount";
    public static final String PK_JAVA_PACKAGE_PRIVATE_MODIFIER_COUNT = "javaPackagePrivateModifierCount";
    public static final String PK_JAVA_PUBLIC_FACTORS = "javaPublicFactors";

    // xml
    public static final String PK_XML_ANDROID_USED_PERMISSIONS = "androidUsedPermissions";
    public static final String PK_XML_POM_DEPENDENCIES = "pomDependencies";

    // api keys
    public static final String PK_AWS_APIKEY_CLIENT_IDS = "awsApiKeyClientIds";
    public static final String PK_AWS_APIKEY_SECRET_KEYS = "awsApiKeySecretKeys";

    // ios
    public static final String PK_IOS_GENERIC_VALUES = "iosGenericValues";

}
