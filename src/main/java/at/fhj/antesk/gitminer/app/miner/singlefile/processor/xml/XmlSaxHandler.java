package at.fhj.antesk.gitminer.app.miner.singlefile.processor.xml;

import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;

public class XmlSaxHandler implements LexicalHandler {

    private SourceFileResults sourceFileResults;

    public XmlSaxHandler(SourceFileResults sourceFileResults) {
        super();

        this.sourceFileResults = sourceFileResults;
    }

    @Override
    public void startDTD(String name, String publicId, String systemId) throws SAXException {

    }

    @Override
    public void endDTD() throws SAXException {

    }

    @Override
    public void startEntity(String name) throws SAXException {

    }

    @Override
    public void endEntity(String name) throws SAXException {

    }

    @Override
    public void startCDATA() throws SAXException {

    }

    @Override
    public void endCDATA() throws SAXException {

    }

    @Override
    public void comment(char[] ch, int start, int length) throws SAXException {
        sourceFileResults.addComment(new String(ch, start, length));
    }
}
