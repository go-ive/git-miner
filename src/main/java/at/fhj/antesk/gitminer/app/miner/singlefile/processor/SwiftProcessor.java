package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.AbstractProcessor;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SwiftProcessor extends AbstractProcessor {

    public SwiftProcessor(SourceFileResults sourceFileResults) {
        super(sourceFileResults);
    }

    @Override
    public void process() {
        super.process();

        String fileContents = sourceFileResults.getSourceFile().getFileContents();

        sourceFileResults.addComments(parseSingleLineComments(fileContents));
        sourceFileResults.addComments(parseMultiLineComments(fileContents));
    }

    private List<String> parseSingleLineComments(String fileContents) {
        List<String> result = new ArrayList<>();

        Pattern pattern = Pattern.compile("^.*?//(.*)$", Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(fileContents);

        while (matcher.find()) {
            String comment = matcher.group(1);
            if (StringUtils.isNotBlank(comment)) {
                result.add(comment);
            }
        }

        return result;
    }

    private List<String> parseMultiLineComments(String fileContents) {
        List<String> result = new ArrayList<>();

        Pattern pattern = Pattern.compile("\\*(.*?)\\*", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(fileContents);

        while (matcher.find()) {
            String comment = matcher.group(1);
            if (StringUtils.isNotBlank(comment)) {
                result.add(comment);
            }
        }

        return result;
    }


}
