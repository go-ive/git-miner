package at.fhj.antesk.gitminer.app.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Map;

public class RawSourceFileResults {

    private String path;
    private String revision;
    private Map<String, Object> data;
    private int version;

    public RawSourceFileResults(String path, String revision, Map<String, Object> data, int version) {
        this.path = path;
        this.revision = revision;
        this.data = data;
        this.version = version;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        RawSourceFileResults that = (RawSourceFileResults) o;

        return new EqualsBuilder()
                .append(version, that.version)
                .append(path, that.path)
                .append(revision, that.revision)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(path)
                .append(revision)
                .append(version)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("path", path)
                .append("revision", revision)
                .append("data", data)
                .append("version", version)
                .toString();
    }


}
