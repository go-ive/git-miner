package at.fhj.antesk.gitminer.app.miner;

/**
 * Executes Miner classes for {@link R} return type and {@link P} parameter type.
 *
 * @param <R> Type that is returned
 * @param <P> Type that is passed
 */
public interface Miner<R, P> {

    R mine(P param);

}
