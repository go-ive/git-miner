package at.fhj.antesk.gitminer.app.dto;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.*;

public class SourceFileResults {

    private int version;
    private SourceFile sourceFile;
    private Map<String, Object> properties = new LinkedHashMap<>();

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public SourceFile getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(SourceFile sourceFile) {
        this.sourceFile = sourceFile;
    }

    // convenience methods

    /**
     * Adds a comments to properties key "comments" if it is not blank or {@code null}.
     *
     * @param comment
     */
    public void addComment(String comment) {
        if (StringUtils.isBlank(comment)) {
            return;
        }

        List<String> comments = (List<String>) properties.get(GitMinerConstants.PK_COMMON_COMMENTS);

        if (comments == null) {
            comments = new ArrayList<>();
            properties.put(GitMinerConstants.PK_COMMON_COMMENTS, comments);
        }

        comments.add(comment);
    }

    public void addComments(List<String> comments) {
        if (comments == null || comments.isEmpty()) {
            return;
        }

        comments.forEach(comment -> addComment(comment));
    }

    public void addToList(String key, String value) {
        if (StringUtils.isBlank(value) || StringUtils.isBlank(key)) {
            return;
        }

        List<String> values = (List<String>) properties.get(key);

        if (values == null) {
            values = new ArrayList<>();
            properties.put(key, values);
        }

        values.add(value);
    }

    public <K, V> void addToMap(String key, K mapKey, V mapValue) {
        if (StringUtils.isBlank(key) || mapKey == null || mapValue == null) {
            return;
        }

        Map<K, V> map = (Map<K, V>) properties.get(key);

        if (map == null) {
            map = new HashMap<>();
            properties.put(key, map);
        }

        map.put(mapKey, mapValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SourceFileResults that = (SourceFileResults) o;

        return new EqualsBuilder()
                .append(version, that.version)
                .append(sourceFile, that.sourceFile)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(version)
                .append(sourceFile)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("properties", properties)
                .append("sourceFile", sourceFile)
                .toString();
    }
}
