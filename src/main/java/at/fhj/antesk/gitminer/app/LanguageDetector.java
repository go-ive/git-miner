package at.fhj.antesk.gitminer.app;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LanguageDetector {

    private static final Map<String, String> languageMap = new HashMap<>();

    static {
        languageMap.put("Html", "Xml");
        languageMap.put("Htm", "Xml");
        languageMap.put("Js", "JavaScript");
        languageMap.put("M", "ObjectiveC");

        Arrays.asList("Png", "Jpg", "Jpeg", "Zip", "Jar", "Bmp", "Tiff")
                .forEach(fileType -> languageMap.put(fileType, GitMinerConstants.IGNORED_FILE_TYPE));
    }

    public static String detect(SourceFile sourceFile) {
        String result = WordUtils.capitalize(FileUtils.extractFileExtension(sourceFile.getName()));

        if (StringUtils.isBlank(result)) {
            return GitMinerConstants.UNKNOWN_FILE_TYPE;
        }

        return languageMap.get(result) == null ? result : languageMap.get(result);
    }

}
