package at.fhj.antesk.gitminer.app.analyzer;

import at.fhj.antesk.gitminer.app.config.Config;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.exception.GitMinerException;
import at.fhj.antesk.gitminer.app.gitapi.GitAPI;
import at.fhj.antesk.gitminer.app.gitapi.GitAPIJGitImpl;
import at.fhj.antesk.gitminer.app.miner.Miner;
import at.fhj.antesk.gitminer.app.miner.SingleFileMiner;
import at.fhj.antesk.gitminer.app.utils.ListUtils;
import at.fhj.antesk.gitminer.persistence.Database;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class PreAnalyzer implements Analyzer {

    private static Logger logger = Logger.getLogger(PreAnalyzer.class);

    private final int MAX_THREADS = Integer.valueOf(Config.getInstance().getProperty(Config.KEYS.ANALYSIS_MAX_THREADS));
    private final boolean PERFORM_CLEANUP = Boolean.valueOf(Config.getInstance().getProperty(Config.KEYS.CLEANUP_AFTER_ANALYSIS));
    private static AtomicInteger progressCounter = new AtomicInteger(0);
    private static AtomicInteger percentage = new AtomicInteger(0);

    private Database database;
    private Miner<SourceFileResults, SourceFile> miner = new SingleFileMiner();
    private RepositoryDTO repositoryDTO;
    private GitAPI gitAPI;

    public PreAnalyzer(RepositoryDTO repositoryDTO, Database database, GitAPI gitAPI) {
        this.repositoryDTO = repositoryDTO;
        this.database = database;
        this.gitAPI = gitAPI;
    }

    public void analyze() {
        logger.info("Running pre-analysis...");

        try {
            database.init();
            final int repositoryId = database.insertRepository(repositoryDTO);

            List<String> revisionHashesForRef = gitAPI.getRevisionHashesForRef(repositoryDTO.getRef());
            Collections.reverse(revisionHashesForRef);

            ListUtils.splitListIntoSubLists(revisionHashesForRef, MAX_THREADS)
                    .parallelStream()
                    .forEach(revisionList -> {
                        GitAPI internalGitApi = createTemporaryGitRepository();

                        for (int i = 0; i < revisionList.size(); i++) {
                            String currentHash = revisionList.get(i);

                            logger.debug("Processing revision  " + currentHash);

                            if (!checkoutRevision(internalGitApi, currentHash)) {
                                continue;
                            }

                            insertCommitMessage(internalGitApi, currentHash, repositoryId);

                            try {
                                int indexInMainList = revisionHashesForRef.indexOf(revisionList.get(i));

                                List<SourceFile> sourceFiles = isFirstRevision(i, indexInMainList)
                                        ? internalGitApi.fetchChangedFilesForCurrentRevision(null, currentHash)
                                        : internalGitApi.fetchChangedFilesForCurrentRevision(revisionHashesForRef.get(indexInMainList - 1), currentHash);

                                sourceFiles.forEach(sourceFile -> processSourceFile(repositoryId, sourceFile));
                            } catch (GitMinerException e) {
                                logger.error(e.getMessage());
                                logger.debug("Could not retrieve list of changed files for revisions", e);
                            }

                            int currentPercentage = ((progressCounter.addAndGet(1) * 100) / revisionHashesForRef.size());
                            if (currentPercentage > percentage.get()) {
                                logger.info("Pre-analysis " + currentPercentage + "% complete");
                                percentage.incrementAndGet();
                            }

                            logger.debug("Done processing revision  " + currentHash);
                        }

                        checkoutMasterAndDeleteLocalBranches(revisionList, internalGitApi);
                    });

            performCleanup();
        } catch (GitMinerException e) {
            logger.error(e.getMessage());
            logger.debug(e.getMessage(), e);
        } finally {
            database.close();
        }
    }

    private void insertCommitMessage(GitAPI internalGitApi, String currentHash, int repositoryId) {
        String commitMessage = internalGitApi.getCommitMessage(currentHash);
        SourceFileResults sourceFileResults = new SourceFileResults();
        SourceFile sourceFile = new SourceFile();
        sourceFile.setRevision(currentHash);
        sourceFile.setName("GitMinerCommitMessageForHash" + currentHash);
        sourceFileResults.setSourceFile(sourceFile);
        sourceFileResults.setVersion(1);
        sourceFileResults.getProperties().put("commitMessage", commitMessage);
        database.insertSourceFileResults(sourceFileResults, repositoryId);
    }

    private void performCleanup() {
        if (PERFORM_CLEANUP) {
            try {
                FileUtils.cleanDirectory(new File(Config.getInstance().getProperty(Config.KEYS.GM_HOME_PATH)));
            } catch (IOException e) {
                logger.error(e.getMessage());
                logger.debug("Error cleaning git miner home directory.", e);
            }
        }
    }

    private boolean isFirstRevision(int i, int indexInMainList) {
        return (i == 0) && (indexInMainList == 0);
    }

    private void processSourceFile(int repositoryId, SourceFile sourceFile) {
        SourceFileResults sourceFileResults = miner.mine(sourceFile);

        if (!sourceFileResults.getProperties().keySet().isEmpty()) {
            database.insertSourceFileResults(sourceFileResults, repositoryId);
        } else {
            logger.debug("No analysis results for " + sourceFile + ". Not persisting.");
        }
    }

    private void checkoutMasterAndDeleteLocalBranches(List<String> revisionList, GitAPI internalGitApi) {
        try {
            internalGitApi.checkoutRevision("master", false);
            internalGitApi.deleteBranch(revisionList.toArray(new String[0]));
        } catch (GitMinerException e) {
            logger.error(e.getMessage());
            logger.debug(e.getMessage(), e);
        }
    }

    private boolean checkoutRevision(GitAPI internalGitApi, String currentHash) {
        try {
            internalGitApi.checkoutRevision(currentHash, true);
        } catch (GitMinerException e) {
            logger.error(e.getMessage());
            logger.debug("Error occurred for hash " + currentHash + ". Skipping...", e);
            return false;
        }

        return true;
    }

    private GitAPI createTemporaryGitRepository() {
        GitAPI gitAPI = null;

        try {
            // copy repository to random generated folder
            File temporaryNewDestination = new File(Config.getInstance().getProperty(Config.KEYS.GM_HOME_PATH) + "/" + RandomStringUtils.randomAlphabetic(10));
            File originalRepository = new File(repositoryDTO.getPath());

            FileUtils.forceMkdir(temporaryNewDestination);
            FileUtils.copyDirectory(originalRepository, temporaryNewDestination);

            gitAPI = new GitAPIJGitImpl(temporaryNewDestination.getAbsolutePath());
            gitAPI.reset();
        } catch (GitMinerException | IOException e) {
            logger.error(e.getMessage());
            logger.debug("Could not copy git repository to temporary directory", e);
            System.exit(1);
        }

        return gitAPI;
    }

}