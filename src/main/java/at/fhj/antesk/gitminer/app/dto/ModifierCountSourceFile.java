package at.fhj.antesk.gitminer.app.dto;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class ModifierCountSourceFile extends AbstractSourceFile {

    private int publicCount;
    private int privateCount;
    private int protectedCount;
    private int packagePrivateCount;

    public int getPublicCount() {
        return publicCount;
    }

    public void setPublicCount(int publicCount) {
        this.publicCount = publicCount;
    }

    public int getPrivateCount() {
        return privateCount;
    }

    public void setPrivateCount(int privateCount) {
        this.privateCount = privateCount;
    }

    public int getProtectedCount() {
        return protectedCount;
    }

    public void setProtectedCount(int protectedCount) {
        this.protectedCount = protectedCount;
    }

    public int getPackagePrivateCount() {
        return packagePrivateCount;
    }

    public void setPackagePrivateCount(int packagePrivateCount) {
        this.packagePrivateCount = packagePrivateCount;
    }

    public double getPublicFactor() {
        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        df.setRoundingMode(RoundingMode.CEILING);

        int everything = privateCount + protectedCount + packagePrivateCount + publicCount;
        if (everything == 0) {
            return 0;
        }

        String format = df.format(publicCount / (double) everything);
        format = format.replaceAll("[^0-9]", ".");

        return Double.valueOf(format);
    }

}
