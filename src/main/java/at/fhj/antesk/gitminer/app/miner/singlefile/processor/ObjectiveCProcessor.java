package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.AbstractProcessor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ObjectiveCProcessor extends AbstractProcessor {

    private static final Pattern REQUEST_ALWAYS_AUTHORIZATION_PATTERN = Pattern
            .compile("\\[(.+) requestAlwaysAuthorization\\]");

    private static final Pattern REQUEST_WHENINUSE_AUTHORIZATION_PATTERN = Pattern
            .compile("\\[(.+) requestWhenInUseAuthorization\\]");

    public ObjectiveCProcessor(SourceFileResults sourceFileResults) {
        super(sourceFileResults);
    }

    @Override
    public void process() {
        super.process();

        String fileContents = sourceFileResults.getSourceFile().getFileContents();

        findAuthorizationRequests(fileContents);
    }

    private void findAuthorizationRequests(String fileContents) {
        Matcher matcher = REQUEST_ALWAYS_AUTHORIZATION_PATTERN.matcher(fileContents);

        while (matcher.find()) {
            String group = matcher.group(1);
            sourceFileResults.addToMap(GitMinerConstants.PK_IOS_GENERIC_VALUES, "requestAlwaysAuthorization", group);
        }

        matcher = REQUEST_WHENINUSE_AUTHORIZATION_PATTERN.matcher(fileContents);

        while (matcher.find()) {
            String group = matcher.group(1);
            sourceFileResults.addToMap(GitMinerConstants.PK_IOS_GENERIC_VALUES, "requestWhenInUseAuthorization", group);
        }
    }
}
