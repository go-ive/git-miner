package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.AbstractProcessor;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

public class PlistProcessor extends AbstractProcessor {

    private static Logger logger = Logger.getLogger(PlistProcessor.class);

    private static final String KEY = "key";
    private static final String DICT = "dict";
    private static final List<String> INTERESTING_KEYS = Arrays.asList("NSLocationAlwaysUsageDescription",
            "NSLocationWhenInUseUsageDescription");

    public PlistProcessor(SourceFileResults sourceFileResults) {
        super(sourceFileResults);
    }

    @Override
    public void process() {
        super.process();

        parseDict();
    }

    private void parseDict() {
        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new StringReader(sourceFileResults.getSourceFile().getFileContents())));

            Node dict = document.getElementsByTagName(DICT).item(0);

            if (dict == null) {
                logger.debug("No dict tags found in " + sourceFileResults.getSourceFile().getName() + " in revision "
                        + sourceFileResults.getSourceFile().getRevision());
                return;
            }

            NodeList dictItems = dict.getChildNodes();
            for (int i = 0; i < dictItems.getLength(); i++) {
                Node currentNode = dictItems.item(i);
                if (KEY.equals(currentNode.getNodeName())) {
                    String keyText = currentNode.getTextContent();

                    if (INTERESTING_KEYS.contains(keyText)) {
                        Node valueNode = dictItems.item(i + 2);
                        String textContent = valueNode.getTextContent();

                        sourceFileResults.addToMap(GitMinerConstants.PK_IOS_GENERIC_VALUES, keyText, textContent);
                    }
                }
            }
        } catch (SAXException | IOException | ParserConfigurationException e) {
            logger.error(e.getMessage());
            logger.debug(e.getMessage(), e);
        }
    }

}
