package at.fhj.antesk.gitminer.app.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SourceFileUtils {

    private static final List<String> KNOWN_URL_PROTOCOLS = Arrays.asList("aaa", "aaas", "about", "acap", "acct", "acr", "adiumxtra",
            "afp", "afs", "aim", "appdata", "apt", "attachment", "aw", "barion", "beshare", "bitcoin", "blob", "bolo", "callto",
            "cap", "chrome", "chrome-extension", "cid", "coap", "coaps", "com-eventbrite-attendee", "content", "crid", "cvs",
            "data", "dav", "dict", "dis", "dlna-playcontainer", "dlna-playsingle", "dns", "dntp", "dtn", "dvb", "ed2k", "example",
            "facetime", "fax", "feed", "feedready", "file", "filesystem", "finger", "fish", "ftp", "geo", "gg", "git", "gizmoproject",
            "go", "gopher", "gtalk", "h323", "ham", "hcp", "http", "https", "iax", "icap", "icon", "im", "imap", "info", "iotdisco",
            "ipn", "ipp", "ipps", "irc", "irc6", "ircs", "iris", "iris.beep", "iris.lwz", "iris.xpc", "iris.xpcs", "isostore", "itms",
            "jabber", "jar", "jms", "keyparc", "lastfm", "ldap", "ldaps", "magnet", "mailserver", "mailto", "maps", "market",
            "message", "mid", "mms", "modem", "ms-access", "ms-browser-extension", "ms-drive-to", "ms-enrollment", "ms-excel",
            "ms-getoffice", "ms-help", "ms-infopath", "ms-media-stream-id", "ms-project", "ms-powerpoint", "ms-publisher",
            "ms-search-repair", "ms-secondary-screen-controller", "ms-secondary-screen-setup", "ms-settings", "ms-settings-airplanemode",
            "ms-settings-bluetooth", "ms-settings-camera", "ms-settings-cellular", "ms-settings-cloudstorage",
            "ms-settings-connectabledevices", "ms-settings-displays-topology", "ms-settings-emailandaccounts", "ms-settings-language",
            "ms-settings-location", "ms-settings-lock", "ms-settings-nfctransactions", "ms-settings-notifications",
            "ms-settings-power", "ms-settings-privacy", "ms-settings-proximity", "ms-settings-screenrotation", "ms-settings-wifi",
            "ms-settings-workplace", "ms-spd", "ms-transit-to", "ms-visio", "ms-walk-to", "ms-word", "msnim", "msrp", "msrps",
            "mtqp", "mumble", "mupdate", "mvn", "news", "nfs", "ni", "nih", "nntp", "notes", "oid", "opaquelocktoken", "pack",
            "palm", "paparazzi", "pkcs11", "platform", "pop", "pres", "prospero", "proxy", "psyc", "query", "redis", "rediss",
            "reload", "res", "resource", "rmi", "rsync", "rtmfp", "rtmp", "rtsp", "rtsps", "rtspu", "secondlife", "service",
            "session", "sftp", "sgn", "shttp", "sieve", "sip", "sips", "skype", "smb", "sms", "smtp", "snews", "snmp", "soap.beep",
            "soap.beeps", "soldat", "spotify", "ssh", "steam", "stun", "stuns", "submit", "svn", "tag", "teamspeak", "tel",
            "teliaeid", "telnet", "tftp", "things", "thismessage", "tip", "tn3270", "tool", "turn", "turns", "tv", "udp",
            "unreal", "urn", "ut2004", "v-event", "vemmi", "ventrilo", "videotex", "vnc", "view-source", "wais", "webcal",
            "wpid", "ws", "wss", "wtai", "wyciwyg", "xcon", "xcon-userid", "xfire", "xmlrpc.beep", "xmlrpc.beeps", "xmpp",
            "xri", "ymsgr", "z39.50", "z39.50r", "z39.50s");

    public static List<String> findAllStrings(String fileContents) {
        List<String> result = new ArrayList<>();

        if (fileContents == null || fileContents.length() < 2) {
            return result;
        }

        Pattern stringPattern = Pattern.compile("(\".*?\")");
        Matcher matcher = stringPattern.matcher(fileContents);
        while (matcher.find()) {
            String match = matcher.group(1);
            match = match.substring(1);
            match = match.substring(0, match.length() - 1);

            result.add(match);
        }

        return result;
    }

    public static boolean isUrl(String possibleUrl) {
        if (possibleUrl == null || possibleUrl.trim().contains(" ")) {
            return false;
        }

        return possibleUrl.startsWith("//") ||
                possibleUrl.contains("://") ||
                possibleUrl.trim().split("\\.").length > 1 ||
                possibleUrl.trim().split("/").length > 1;
    }

    public static String parseAbsoluteUrl(String possibleUrl) {
        String firstFoundProtocol = null;

        for (String protocol : KNOWN_URL_PROTOCOLS) {
            if (possibleUrl.contains(protocol + "://")) {
                firstFoundProtocol = protocol;
                break;
            }
        }

        if (firstFoundProtocol == null) {
            return null;
        }

        String[] parts = possibleUrl.split(firstFoundProtocol);

        if (parts.length < 2) {
            return null;
        }

        String resultPart = parts[1];

        if (parts[1].contains("<")) {
            resultPart = resultPart.substring(0, resultPart.indexOf("<"));
        } else if (parts[1].contains(">")) {
            resultPart = resultPart.substring(0, resultPart.indexOf(">"));
        } else if (parts[1].contains("\"")) {
            resultPart = resultPart.substring(0, resultPart.indexOf("\""));
        }

        return firstFoundProtocol + resultPart;
    }

}
