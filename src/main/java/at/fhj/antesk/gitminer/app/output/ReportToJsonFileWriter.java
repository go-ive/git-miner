package at.fhj.antesk.gitminer.app.output;

import at.fhj.antesk.gitminer.app.dto.Report;
import at.fhj.antesk.gitminer.app.utils.JsonUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

public class ReportToJsonFileWriter implements OutputWriter<Void> {

    private static Logger logger = Logger.getLogger(ReportToJsonFileWriter.class);

    private Report report;

    public ReportToJsonFileWriter(Report report) {
        this.report = report;
    }

    @Override
    public Void write() {
        logger.info("Writing report to json file...");

        logger.info("urls: " + (report.getData().get("urls") != null ? ((Collection)report.getData().get("urls")).size() : 0));
        logger.info("emailAddresses: " + (report.getData().get("emailAddresses") != null ? ((Collection)report.getData().get("emailAddresses")).size() : 0));
        logger.info("pomDependencies: " + (report.getData().get("pomDependencies") != null ? ((Collection)report.getData().get("pomDependencies")).size(): 0));
        logger.info("comments: " + (report.getData().get("comments") != null ? ((Collection)report.getData().get("comments")).size(): 0));
        logger.info("androidUsedPermissions: " + (report.getData().get("androidUsedPermissions") != null ? ((Collection)report.getData().get("androidUsedPermissions")).size(): 0));

        String result = JsonUtils.getInstance().gson.toJson(report);

        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String timestamp = sdf.format(currentDate);

        String fileName = "Report_" + report.getRepositoryId() + "-" + timestamp + ".json";
        try {
            FileUtils.write(new File(fileName), result);
            logger.info("Successfully written report: " + fileName);
        } catch (IOException e) {
            logger.error(e.getMessage());
            logger.debug(e.getMessage(), e);
        }

        return null;
    }
}
