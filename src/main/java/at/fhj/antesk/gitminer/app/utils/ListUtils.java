package at.fhj.antesk.gitminer.app.utils;

import at.fhj.antesk.gitminer.app.config.Config;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

    /**
     * Splits a list of T into a list of lists which have overlapping items.
     *
     * @param source The list to split
     * @param lists  Amount of lists to generate
     * @param <T>
     * @return List of overlapping lists
     */
    public static <T> List<List<T>> splitListIntoOverlappingSubLists(List<T> source, int lists) {
        final List<List<T>> result = new ArrayList<>();

        int segmentSize = source.size() / lists;
        if (source.size() % lists > 0) {
            segmentSize++;
        }

        for (int segment = 0; segment < lists; segment++) {
            final List<T> segmentList = new ArrayList<>();

            for (int item = segment * segmentSize - segment; item < (segment * segmentSize - segment) + segmentSize; item++) {
                segmentList.add(source.get(item));
            }

            result.add(segmentList);
        }

        return result;
    }

    /**
     * Creates a list of pairs of elements from the source list. The elements are overlapping by one.
     *
     * @param source
     * @param <T>
     * @return List of pairs with source.size() - 1 in size
     */
    public static <T> List<Pair<T, T>> createListOfPairs(List<T> source) {
        final List<Pair<T, T>> result = new ArrayList<>();

        for (int i = 0; i < source.size(); i++) {
            T current = source.get(i);

            try {
                T next = source.get(i + 1);
                result.add(Pair.of(current, next));
            } catch (IndexOutOfBoundsException e) {
                break; // last element reached
            }
        }

        return result;
    }

    public static <T> List<List<T>> splitListIntoSubLists(List<T> source, int threads) {
        int partitionSize = source.size() / threads;

        if (partitionSize < 1) {
            Config.getInstance().setProperty(Config.KEYS.ANALYSIS_MAX_THREADS, 1);
            partitionSize = source.size();
        }

        return org.apache.commons.collections4.ListUtils.partition(source, partitionSize);
    }
}
