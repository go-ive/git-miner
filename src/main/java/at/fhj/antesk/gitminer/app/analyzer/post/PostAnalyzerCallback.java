package at.fhj.antesk.gitminer.app.analyzer.post;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.ModifierCountSourceFile;
import at.fhj.antesk.gitminer.app.dto.RawSourceFileResults;
import at.fhj.antesk.gitminer.app.dto.Report;
import at.fhj.antesk.gitminer.persistence.DatabaseCallback;
import org.apache.log4j.Logger;

import java.util.*;

public class PostAnalyzerCallback implements DatabaseCallback<RawSourceFileResults> {

    private static Logger logger = Logger.getLogger(PostAnalyzerCallback.class);

    private static final String JAVA_FILE_TYPE = "Java";

    private Report report;

    private Map<String, Set<String>> fileChangesInRevisions = new HashMap<>();
    private List<ModifierCountSourceFile> modifierCountSourceFiles = new ArrayList<>();

    public Map<String, Set<String>> getFileChangesInRevisions() {
        return fileChangesInRevisions;
    }

    public List<ModifierCountSourceFile> getModifierCountSourceFiles() {
        return modifierCountSourceFiles;
    }

    public PostAnalyzerCallback(Report report) {
        this.report = report;
    }

    @Override
    public void callback(RawSourceFileResults rawSourceFileResults) {
        logger.debug("Post-processing: " + rawSourceFileResults.getPath() + ":" + rawSourceFileResults.getRevision());

        fillCollections(rawSourceFileResults);
        addRevisionToChangedFile(rawSourceFileResults);

        String fileType = (String) rawSourceFileResults.getData().get(GitMinerConstants.PK_COMMON_FILE_TYPE);
        if (JAVA_FILE_TYPE.equals(fileType)) {
            addModifierCounts(rawSourceFileResults);
        }

        if (rawSourceFileResults.getPath().startsWith("GitMinerCommitMessageForHash")) {
            String commitMessage = (String) rawSourceFileResults.getData().get("commitMessage");
            Map<String, String> commitMessages = (Map<String, String>) report.getData().get("commitMessages");
            if (commitMessages == null) {
                commitMessages = new HashMap<>();
                report.getData().put("commitMessages", commitMessages);
            }
            commitMessages.put(rawSourceFileResults.getRevision(), commitMessage);
        }
    }

    private void addModifierCounts(RawSourceFileResults rawSourceFileResults) {
        ModifierCountSourceFile modifierCountSourceFile = new ModifierCountSourceFile();
        modifierCountSourceFile.setName(rawSourceFileResults.getPath());
        modifierCountSourceFile.setRevision(rawSourceFileResults.getRevision());

        Map<String, Object> data = rawSourceFileResults.getData();

        Double publicModifierCount = (Double) data.get(GitMinerConstants.PK_JAVA_PUBLIC_MODIFIER_COUNT);
        Double protectedModifierCount = (Double) data.get(GitMinerConstants.PK_JAVA_PROTECTED_MODIFIER_COUNT);
        Double privateModifierCount = (Double) data.get(GitMinerConstants.PK_JAVA_PRIVATE_MODIFIER_COUNT);
        Double packagePrivateModifierCount = (Double) data.get(GitMinerConstants.PK_JAVA_PACKAGE_PRIVATE_MODIFIER_COUNT);

        publicModifierCount = publicModifierCount == null ? 0 : publicModifierCount;
        protectedModifierCount = protectedModifierCount == null ? 0 : protectedModifierCount;
        privateModifierCount = privateModifierCount == null ? 0 : privateModifierCount;
        packagePrivateModifierCount = packagePrivateModifierCount == null ? 0 : packagePrivateModifierCount;

        modifierCountSourceFile.setPublicCount(publicModifierCount.intValue());
        modifierCountSourceFile.setProtectedCount(protectedModifierCount.intValue());
        modifierCountSourceFile.setPrivateCount(privateModifierCount.intValue());
        modifierCountSourceFile.setPackagePrivateCount(packagePrivateModifierCount.intValue());

        modifierCountSourceFiles.add(modifierCountSourceFile);
    }

    private void fillCollections(RawSourceFileResults rawSourceFileResults) {
        Map<String, Object> data = rawSourceFileResults.getData();
        for (String key : data.keySet()) {
            Object value = data.get(key);

            if (value instanceof Collection) {
                ((Collection) value).forEach(object -> {
                    Set set = (Set) report.getData().get(key);

                    if (set == null) {
                        set = new HashSet();
                        report.getData().put(key, set);
                    }

                    set.add(object);
                });
            }

            if (value instanceof Map) {
                Map map = (Map) report.getData().get(key);

                if (map == null) {
                    map = new HashMap();
                    report.getData().put(key, map);
                }

                Map valueMap = ((Map) value);
                for (Object k : valueMap.keySet()) {
                    map.put(k, valueMap.get(k));
                }
            }
        }
    }

    private void addRevisionToChangedFile(RawSourceFileResults rawSourceFileResults) {
        String path = rawSourceFileResults.getPath();

        Set<String> revisions = fileChangesInRevisions.get(path);
        if (revisions == null) {
            revisions = new HashSet<>();
            fileChangesInRevisions.put(path, revisions);
        }

        revisions.add(rawSourceFileResults.getRevision());
    }

}
