package at.fhj.antesk.gitminer.app.miner.singlefile.processor.java;

import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.EnumConstantDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.lang.reflect.Modifier;

import static at.fhj.antesk.gitminer.app.config.GitMinerConstants.*;

public class ASTVisitor extends VoidVisitorAdapter<Object> {

    private SourceFileResults sourceFileResults;

    public void setSourceFileResults(SourceFileResults sourceFileResults) {
        this.sourceFileResults = sourceFileResults;
    }

    @Override
    public void visit(ClassOrInterfaceDeclaration node, Object arg) {
        super.visit(node, arg);

        String entityType = node.isInterface() ? ENTITY_TYPE_INTERFACE : ENTITY_TYPE_CLASS;
        sourceFileResults.getProperties().put(PK_JAVA_ENTITY_TYPE, entityType);

        updateModifierCount(node.getModifiers());
    }

    @Override
    public void visit(EnumConstantDeclaration node, Object arg) {
        super.visit(node, arg);

        sourceFileResults.getProperties().put(PK_JAVA_ENTITY_TYPE, ENTITY_TYPE_ENUM);
    }

    @Override
    public void visit(PackageDeclaration n, Object arg) {
        super.visit(n, arg);

        sourceFileResults.getProperties().put(PK_JAVA_PACKAGES, n.getName().toString());
    }

    @Override
    public void visit(LineComment n, Object arg) {
        super.visit(n, arg);

        sourceFileResults.addComment(n.getContent());
    }

    @Override
    public void visit(BlockComment n, Object arg) {
        super.visit(n, arg);

        sourceFileResults.addComment(n.getContent());
    }

    @Override
    public void visit(JavadocComment n, Object arg) {
        super.visit(n, arg);

        sourceFileResults.addComment(n.getContent());
    }

    @Override
    public void visit(MethodDeclaration n, Object arg) {
        super.visit(n, arg);

        updateModifierCount(n.getModifiers());
    }

    @Override
    public void visit(FieldDeclaration n, Object arg) {
        super.visit(n, arg);

        updateModifierCount(n.getModifiers());
    }

    private void incrementCount(String propertyKey) {
        Integer count = (Integer) sourceFileResults.getProperties().get(propertyKey);

        if (count != null) {
            sourceFileResults.getProperties().put(propertyKey, ++count);
        } else {
            sourceFileResults.getProperties().put(propertyKey, 1);
        }
    }

    private void updateModifierCount(int modifiers) {
        if (Modifier.isPrivate(modifiers)) {
            incrementCount(PK_JAVA_PRIVATE_MODIFIER_COUNT);
        } else if (Modifier.isPublic(modifiers)) {
            incrementCount(PK_JAVA_PUBLIC_MODIFIER_COUNT);
        } else if (Modifier.isProtected(modifiers)) {
            incrementCount(PK_JAVA_PROTECTED_MODIFIER_COUNT);
        } else {
            incrementCount(PK_JAVA_PACKAGE_PRIVATE_MODIFIER_COUNT);
        }
    }
}
