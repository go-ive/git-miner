package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.AbstractProcessor;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertiesProcessor extends AbstractProcessor {

    private static Logger logger = Logger.getLogger(PropertiesProcessor.class);

    public PropertiesProcessor(SourceFileResults sourceFileResults) {
        super(sourceFileResults);
    }

    @Override
    public void process() {
        super.process();

        SourceFile sourceFile = sourceFileResults.getSourceFile();

        parseComments(sourceFile);

        Properties properties = new Properties();

        try {
            properties.load(new StringReader(sourceFile.getFileContents()));
        } catch (IOException e) {
            logger.error(e.getMessage());
            logger.debug("Could not load properties from file contents for " + sourceFile.getName(), e);
            return;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.debug("En error occurred while parsing " + sourceFile.getName(), e);
            return;
        }

        sourceFileResults.getProperties().put("keyCount", properties.size());
    }

    private void parseComments(SourceFile sourceFile) {
        Pattern pattern = Pattern.compile("\\#(.*)");
        Matcher matcher = pattern.matcher(sourceFile.getFileContents());

        while (matcher.find()) {
            if (matcher.group(1) != null) {
                sourceFileResults.addComment(matcher.group(1));
            }
        }
    }

}
