package at.fhj.antesk.gitminer.app.output;

public interface OutputWriter<O> {

    O write();

}
