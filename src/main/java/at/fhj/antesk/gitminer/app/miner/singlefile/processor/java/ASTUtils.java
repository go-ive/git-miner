package at.fhj.antesk.gitminer.app.miner.singlefile.processor.java;

import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.TokenMgrError;
import com.github.javaparser.ast.CompilationUnit;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class ASTUtils {

    private static Logger logger = Logger.getLogger(ASTUtils.class);

    public static void parse(String fullPathOfFile, SourceFileResults sourceFileResults) {
        try {
            CompilationUnit compilationUnit = JavaParser.parse(new File(fullPathOfFile));

            ASTVisitor visitor = new ASTVisitor();
            visitor.setSourceFileResults(sourceFileResults);

            visitor.visit(compilationUnit, null);
        } catch (ParseException | IOException e) {
            logger.error(e.getMessage());
            logger.debug("Error occurred during AST parsing for file " + sourceFileResults.getSourceFile().getName(), e);
        } catch (TokenMgrError e) {
            logger.fatal(e.getMessage());
            logger.debug("Fatal error occurred for file " + sourceFileResults.getSourceFile().getName(), e);
        }
    }

}
