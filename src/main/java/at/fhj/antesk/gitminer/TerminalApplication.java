package at.fhj.antesk.gitminer;

import at.fhj.antesk.gitminer.app.ApplicationFacade;
import at.fhj.antesk.gitminer.app.config.Config;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.exception.GitMinerException;
import at.fhj.antesk.gitminer.terminal.OptionCreator;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

public class TerminalApplication {

    private static Logger logger = Logger.getLogger(TerminalApplication.class);

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        Options options = OptionCreator.createOptions();

        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse(options, args);
            execute(commandLine);
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(100, "./git-miner -path /path/to/git/repository -o runAnalysis -name MyGitRepository -ref <custom tag/ref/commithash>", null, options, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        stopWatch.stop();
        Date executionTime = new Date(stopWatch.getTime());

        logger.info("Execution of " + Arrays.asList(args) + " took " + sdf.format(executionTime));
    }

    private static void execute(CommandLine commandLine) throws IOException, GitMinerException {
        String path = commandLine.getOptionValue("path");
        Config.getInstance().setProperty(Config.KEYS.CLI_PARAM_REPOSITORY_PATH, path);

        ApplicationFacade applicationFacade = new ApplicationFacade(new RepositoryDTO(
                commandLine.getOptionValue("name") == null ? "PLACEHOLDER" : commandLine.getOptionValue("name"),
                commandLine.getOptionValue("url") == null ? "PLACEHOLDER" : commandLine.getOptionValue("url"),
                path,
                commandLine.getOptionValue("ref") == null ? "HEAD" : commandLine.getOptionValue("ref")),
                commandLine.hasOption("testMode"));

        applicationFacade.run();
    }

}
