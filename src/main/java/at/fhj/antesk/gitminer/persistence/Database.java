package at.fhj.antesk.gitminer.persistence;

import at.fhj.antesk.gitminer.app.dto.RawSourceFileResults;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;

public interface Database {

    void init();

    void close();

    int insertRepository(RepositoryDTO repositoryDTO);

    void insertSourceFileResults(SourceFileResults sourceFileResults, int repositoryId);

    void processResults(RepositoryDTO repositoryDTO, int version, DatabaseCallback<RawSourceFileResults> databaseCallback);

}
