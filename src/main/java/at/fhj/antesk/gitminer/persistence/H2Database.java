package at.fhj.antesk.gitminer.persistence;

import at.fhj.antesk.gitminer.app.config.Config;
import at.fhj.antesk.gitminer.app.dto.RawSourceFileResults;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.utils.JsonUtils;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Map;

public class H2Database extends AbstractDatabase implements Database {

    private static Logger logger = Logger.getLogger(H2Database.class);
    private static final int BATCH_SIZE = 10000;

    private Connection connection;
    private boolean isSetup;

    public H2Database() {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
            logger.debug("Could not instantiate h2 driver.", e);
            System.exit(1);
        }
    }

    protected synchronized Connection getConnection() throws SQLException {
        String dbUrl = Config.getInstance().getProperty(Config.KEYS.H2_DB_URL);
        String user = Config.getInstance().getProperty(Config.KEYS.H2_DB_USER);
        String password = Config.getInstance().getProperty(Config.KEYS.H2_DB_PASSWORD);

        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(dbUrl, user, password);
            if (!isSetup) {
                setupDb();
            }
        }

        return connection;
    }

    @Override
    public void init() {
        try {
            getConnection();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.debug("Could not open connection", e);
        }
    }

    private void setupDb() {
        try {
            Statement statement = getConnection().createStatement();

            getConnection().setAutoCommit(true);

            statement.executeUpdate("DROP TABLE IF EXISTS source_file_results");
            statement.executeUpdate("DROP TABLE IF EXISTS repository");
            statement.executeUpdate("CREATE TABLE repository (" +
                    "id SERIAL," +
                    "name VARCHAR(100) NOT NULL," +
                    "url VARCHAR(1000) NULL," +
                    "directory VARCHAR(5000) NOT NULL," +
                    "ref VARCHAR(500) NOT NULL," +
                    "CONSTRAINT pk_repository_id PRIMARY KEY (id)" +
                    ")");
            statement.executeUpdate("CREATE TABLE source_file_results (" +
                    "    id SERIAL," +
                    "    repository_id INT REFERENCES repository(id)," +
                    "    file_path VARCHAR(5000) NOT NULL," +
                    "    revision VARCHAR(40) NOT NULL," +
                    "    data VARCHAR(100000) NOT NULL," +
                    "    version INT NOT NULL," +
                    "    CONSTRAINT pk_source_file_results_id PRIMARY KEY (id)," +
                    "    UNIQUE (repository_id, file_path, revision, version)" +
                    ")");

            statement.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

        isSetup = true;
    }

    @Override
    public void close() {
        try {
            getConnection().close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.debug("Error closing connection", e);
        }
    }

    @Override
    public void insertSourceFileResults(SourceFileResults sourceFileResults, int repositoryId) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(INSERT_STMT);
            preparedStatement.setInt(1, repositoryId);
            preparedStatement.setString(2, sourceFileResults.getSourceFile().getName());
            preparedStatement.setString(3, sourceFileResults.getSourceFile().getRevision());
            preparedStatement.setString(4, JsonUtils.getInstance().gson.toJson(sourceFileResults.getProperties()));
            preparedStatement.setInt(5, sourceFileResults.getVersion());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            handleSourceFileResultsException(sourceFileResults, e);
        }
    }


    @Override
    public void processResults(RepositoryDTO repositoryDTO, int version, DatabaseCallback<RawSourceFileResults> databaseCallback) {
        try {
            getConnection().setAutoCommit(false);

            PreparedStatement preparedStatement = getConnection()
                    .prepareStatement("SELECT file_path, revision, data FROM source_file_results WHERE repository_id = ? and version = ?");
            preparedStatement.setFetchSize(BATCH_SIZE);

            preparedStatement.setInt(1, internalRetrieveId(repositoryDTO));
            preparedStatement.setInt(2, version);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                databaseCallback.callback(new RawSourceFileResults(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        JsonUtils.getInstance().gson.fromJson(resultSet.getString(3), Map.class),
                        version));
            }

            resultSet.close();
            preparedStatement.close();

            getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.debug("postAnalyze sql error", e);
        }
    }

}
