package at.fhj.antesk.gitminer.persistence;

import at.fhj.antesk.gitminer.app.config.Config;
import at.fhj.antesk.gitminer.app.dto.RawSourceFileResults;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.utils.JsonUtils;
import org.apache.log4j.Logger;
import org.postgresql.util.PGobject;

import java.sql.*;
import java.util.Map;

public class PostgresDatabase extends AbstractDatabase implements Database {

    private static Logger logger = Logger.getLogger(Database.class);

    private static final String JSON = "json";
    private static final int BATCH_SIZE = 10000;

    private Connection connection;

    public PostgresDatabase() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
            logger.debug("Could not instantiate postgres driver.", e);
            System.exit(1);
        }
    }

    protected Connection getConnection() throws SQLException {
        String dbUrl = Config.getInstance().getProperty(Config.KEYS.POSTGRES_DB_URL);
        String user = Config.getInstance().getProperty(Config.KEYS.POSTGRES_DB_USER);
        String password = Config.getInstance().getProperty(Config.KEYS.POSTGRES_DB_PASSWORD);

        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(dbUrl, user, password);
        }

        return connection;
    }

    @Override
    public void init() {
        try {
            getConnection();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.debug("Could not open connection", e);
        }
    }

    @Override
    public void close() {
        try {
            getConnection().close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.debug("Error closing connection", e);
        }
    }

    @Override
    public void insertSourceFileResults(SourceFileResults sourceFileResults, int repositoryId) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(INSERT_STMT);
            preparedStatement.setInt(1, repositoryId);
            preparedStatement.setString(2, sourceFileResults.getSourceFile().getName());
            preparedStatement.setString(3, sourceFileResults.getSourceFile().getRevision());

            PGobject dataObject = new PGobject();
            dataObject.setType(JSON);
            dataObject.setValue(JsonUtils.getInstance().gson.toJson(sourceFileResults.getProperties()));
            preparedStatement.setObject(4, dataObject);

            preparedStatement.setInt(5, sourceFileResults.getVersion());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            handleSourceFileResultsException(sourceFileResults, e);
        }
    }

    @Override
    public void processResults(RepositoryDTO repositoryDTO, int version, DatabaseCallback<RawSourceFileResults> databaseCallback) {
        try {
            getConnection().setAutoCommit(false);

            PreparedStatement preparedStatement = getConnection()
                    .prepareStatement("SELECT file_path, revision, data FROM source_file_results WHERE repository_id = ? and version = ?");
            preparedStatement.setFetchSize(BATCH_SIZE);

            preparedStatement.setInt(1, internalRetrieveId(repositoryDTO));
            preparedStatement.setInt(2, version);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                databaseCallback.callback(new RawSourceFileResults(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        JsonUtils.getInstance().gson.fromJson(resultSet.getString(3), Map.class),
                        version));
            }

            resultSet.close();
            preparedStatement.close();

            getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.debug("postAnalyze sql error", e);
        }
    }

}
