package at.fhj.antesk.gitminer.persistence;

import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.apache.log4j.Logger;

import java.sql.*;

public abstract class AbstractDatabase {

    protected static Logger logger = Logger.getLogger(Database.class);

    private static final String UNIQUE_CONSTRAINT_VIOLATION_STATE = "23505";
    protected static final String INSERT_STMT = "INSERT INTO source_file_results (repository_id, file_path, revision, data, version) VALUES (?,?,?,?,?)";

    protected abstract Connection getConnection() throws SQLException;

    protected int internalRetrieveId(RepositoryDTO repositoryDTO) throws SQLException {
        PreparedStatement preparedStatement = getConnection()
                .prepareStatement("SELECT id FROM repository WHERE name = ? and url = ? and directory = ? and ref = ?");
        preparedStatement.setString(1, repositoryDTO.getName());
        preparedStatement.setString(2, repositoryDTO.getUrl());
        preparedStatement.setString(3, repositoryDTO.getPath());
        preparedStatement.setString(4, repositoryDTO.getRef());

        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next() ? resultSet.getInt(1) : -1;
    }

    public int insertRepository(RepositoryDTO repositoryDTO) {
        try {
            int id = internalRetrieveId(repositoryDTO);

            if (id != -1) {
                logger.debug("Repository with id " + id + " already exists");
                return id;
            }

            ResultSet generatedKeys = insertRepositoryAndReturnGeneratedKeys(repositoryDTO);
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.debug("Error saving repository " + repositoryDTO, e);
        }

        return -1;
    }

    protected ResultSet insertRepositoryAndReturnGeneratedKeys(RepositoryDTO repositoryDTO) throws SQLException {
        PreparedStatement preparedStatement = getConnection()
                .prepareStatement("INSERT INTO repository (name, url, directory, ref) VALUES (?,?,?,?)",
                        Statement.RETURN_GENERATED_KEYS);

        preparedStatement.setString(1, repositoryDTO.getName());
        preparedStatement.setString(2, repositoryDTO.getUrl());
        preparedStatement.setString(3, repositoryDTO.getPath());
        preparedStatement.setString(4, repositoryDTO.getRef());
        preparedStatement.executeUpdate();

        return preparedStatement.getGeneratedKeys();
    }

    protected void handleSourceFileResultsException(SourceFileResults sourceFileResults, SQLException e) {
        switch (e.getSQLState()) {
            case UNIQUE_CONSTRAINT_VIOLATION_STATE:
                logger.warn("Entry for " + sourceFileResults.getSourceFile().getName() + " v" + sourceFileResults.getVersion() + " already exists.");
                break;
            default:
                logger.error(e.getMessage());
                logger.debug("Could not save source file results for " + sourceFileResults, e);
                break;
        }
    }

}
