package at.fhj.antesk.gitminer.persistence;

public interface DatabaseCallback<T> {

    void callback(T param);

}
