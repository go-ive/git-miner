package at.fhj.antesk.gitminer.app.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class FileUtilsCountFilesPerPathTest {

    private List<String> pathList = Arrays.asList(
            "/a/b/1.txt", "/a/b/2.txt", "/a/b/3.txt",
            "/a/1.txt", "/a/2.txt",
            "1.txt", "2.txt", "3.txt", "4.txt"
    );

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNull() {
        FileUtils.countFilesPerPath(null);
    }

    @Test
    public void shouldCountCorrectly() {
        Map<String, Integer> filesPerPath = FileUtils.countFilesPerPath(pathList);

        Assert.assertEquals("Incorrect amount of files", Integer.valueOf(3), filesPerPath.get("/a/b/"));
        Assert.assertEquals("Incorrect amount of files", Integer.valueOf(2), filesPerPath.get("/a/"));
        Assert.assertEquals("Incorrect amount of files", Integer.valueOf(4), filesPerPath.get(""));
    }

}
