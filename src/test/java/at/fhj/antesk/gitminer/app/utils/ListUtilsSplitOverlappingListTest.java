package at.fhj.antesk.gitminer.app.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(Parameterized.class)
public class ListUtilsSplitOverlappingListTest {

    private int listSize;
    private int amountOfLists;

    public ListUtilsSplitOverlappingListTest(int listSize, int amountOfLists) {
        this.listSize = listSize;
        this.amountOfLists = amountOfLists;
    }

    @Parameterized.Parameters(name = "List of size {0} with {1} sub lists.")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {5, 1},
                {4, 2},
                {10, 3},
                {100, 7},
                {347, 49},
                {550000, 16}
        });
    }

    @Test
    public void shouldSplitList() {
        List<String> input = new ArrayList<>();
        for (int i = 1; i <= listSize; i++) {
            input.add(i + "");
        }

        List<List<String>> output = ListUtils.splitListIntoOverlappingSubLists(input, amountOfLists);

        Assert.assertEquals(amountOfLists, output.size());

        List<String> first = output.get(0);

        if (output.size() == 1) {
            Assert.assertEquals(listSize, first.size());
        } else {
            List<String> second = output.get(1);

            Assert.assertEquals(first.get(first.size() - 1), second.get(0));
        }
    }

}
