package at.fhj.antesk.gitminer.app;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import org.junit.Assert;
import org.junit.Test;

public class LanguageDetectorTest {

    @Test
    public void shouldDetectJava() {
        SourceFile sourceFile = new SourceFile();
        sourceFile.setName("MyClass.java");

        String language = LanguageDetector.detect(sourceFile);

        Assert.assertEquals("Wrong language detected", "Java", language);
    }

    @Test
    public void shouldFailToDetectLanguage() {
        SourceFile sourceFile = new SourceFile();
        sourceFile.setName("MyClass");

        String language = LanguageDetector.detect(sourceFile);

        Assert.assertEquals("Wrong language detected", "Unknown", language);
    }

    @Test
    public void shouldReturnActualLanguage() {
        SourceFile sourceFile = new SourceFile();
        sourceFile.setName("something.html");

        Assert.assertEquals("Wrong language detected", "Xml", LanguageDetector.detect(sourceFile));
    }

    @Test
    public void shouldReturnActualLanguageWhenSame() {
        SourceFile sourceFile = new SourceFile();
        sourceFile.setName("something.xml");

        Assert.assertEquals("Wrong language detected", "Xml", LanguageDetector.detect(sourceFile));
    }

    @Test
    public void shouldReturnIgnoredType() {
        SourceFile sourceFile = new SourceFile();
        sourceFile.setName("something.png");

        Assert.assertEquals("Wrong language detected", GitMinerConstants.IGNORED_FILE_TYPE, LanguageDetector.detect(sourceFile));
    }

}
