package at.fhj.antesk.gitminer.app.utils;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListUtilsCreatePairsTest {

    @Test
    public void shouldCreatePairsOfItems() {
        List<String> sourceList = new ArrayList<>();
        sourceList.add("First");
        sourceList.add("Second");
        sourceList.add("Third");

        List<Pair<String, String>> listOfPairs = ListUtils.createListOfPairs(sourceList);

        Assert.assertEquals(2, listOfPairs.size());

        Assert.assertEquals("First", listOfPairs.get(0).getLeft());
        Assert.assertEquals("Second", listOfPairs.get(0).getRight());

        Assert.assertEquals("Second", listOfPairs.get(1).getLeft());
        Assert.assertEquals("Third", listOfPairs.get(1).getRight());
    }

}
