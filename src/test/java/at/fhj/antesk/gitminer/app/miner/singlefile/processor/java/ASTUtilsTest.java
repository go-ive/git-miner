package at.fhj.antesk.gitminer.app.miner.singlefile.processor.java;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ASTUtilsTest {

    private static final String CLASS_FILE = "src/test/resources/javaprocessor/ApiClientImpl.java";
    private static final String INTERFACE_FILE = "src/test/resources/javaprocessor/ApiClient.java";
    private static final String ENUM_FILE = "src/test/resources/javaprocessor/Type.java";

    @Test
    public void shouldRecognizeClass() {
        SourceFileResults sourceFileResults = new SourceFileResults();

        ASTUtils.parse(CLASS_FILE, sourceFileResults);

        assertEquals(GitMinerConstants.ENTITY_TYPE_CLASS,
                sourceFileResults.getProperties().get(GitMinerConstants.PK_JAVA_ENTITY_TYPE));
    }

    @Test
    public void shouldRecognizeInterface() {
        SourceFileResults sourceFileResults = new SourceFileResults();

        ASTUtils.parse(INTERFACE_FILE, sourceFileResults);

        assertEquals(GitMinerConstants.ENTITY_TYPE_INTERFACE,
                sourceFileResults.getProperties().get(GitMinerConstants.PK_JAVA_ENTITY_TYPE));
    }

    @Test
    public void shouldRecognizeEnum() {
        SourceFileResults sourceFileResults = new SourceFileResults();

        ASTUtils.parse(ENUM_FILE, sourceFileResults);

        assertEquals(GitMinerConstants.ENTITY_TYPE_ENUM,
                sourceFileResults.getProperties().get(GitMinerConstants.PK_JAVA_ENTITY_TYPE));
    }
}
