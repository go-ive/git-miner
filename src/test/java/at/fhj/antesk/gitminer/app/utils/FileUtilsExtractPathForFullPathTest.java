package at.fhj.antesk.gitminer.app.utils;

import org.junit.Assert;
import org.junit.Test;

public class FileUtilsExtractPathForFullPathTest {

    private static final String PATH = "./a/b/c.txt";

    @Test
    public void shouldNotReturnNull() {
        String path = FileUtils.extractPathForFullPath(PATH);

        Assert.assertNotNull(path);
    }

    @Test
    public void shouldReturnPathForAbsolutePath() {
        String absolutePath = "/a/b/c.txt";
        String path = FileUtils.extractPathForFullPath(absolutePath);

        Assert.assertEquals("Path not correct", "/a/b/", path);
    }

    @Test
    public void shouldReturnPathForRelativePath() {
        String relativePath = "a/b/c.txt";
        String path = FileUtils.extractPathForFullPath(relativePath);

        Assert.assertEquals("Path not correct", "a/b/", path);
    }

    @Test
    public void shouldReturnEmptyPathForRootFile() {
        String emptyPath = "test.txt";
        String path = FileUtils.extractPathForFullPath(emptyPath);

        Assert.assertEquals("Path not correct", "", path);
    }

}
