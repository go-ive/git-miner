package at.fhj.antesk.gitminer.app.miner;

import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.exception.NoSuchProcessorException;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.DefaultProcessor;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.JavaProcessor;
import at.fhj.antesk.gitminer.app.miner.singlefile.processor.common.Processor;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SingleFileMinerTest {

    private SingleFileMiner miner = new SingleFileMiner();

    @Test
    public void shouldGetCorrectProcessor() throws NoSuchProcessorException {
        SourceFile sourceFile = new SourceFile();
        sourceFile.setName("test.java");

        SourceFileResults sourceFileResults = new SourceFileResults();
        sourceFileResults.setSourceFile(sourceFile);
        Processor processor = miner.getProcessor(sourceFileResults, "Java");

        assertTrue(processor instanceof JavaProcessor);
    }

    @Test
    public void shouldHandleNotExistingProcessor() throws NoSuchProcessorException {
        SourceFile sourceFile = new SourceFile();
        sourceFile.setName("test.unknown");

        SourceFileResults sourceFileResults = new SourceFileResults();
        sourceFileResults.setSourceFile(sourceFile);
        Processor processor = miner.getProcessor(sourceFileResults, "Unknown");

        assertTrue(processor instanceof DefaultProcessor);
    }

}
