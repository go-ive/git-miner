package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class SwiftProcessorTest {

    private SourceFileResults sourceFileResults;

    @Before
    public void setUp() throws IOException {
        sourceFileResults = new SourceFileResults();

        SourceFile sourceFile = new SourceFile();
        sourceFile.setName("SingleCommentFile.swift");
        sourceFile.setFileContents("@UIApplicationMain\n" +
                "class AppDelegate: UIResponder, UIApplicationDelegate {\n" +
                "    \n" +
                "    var window: UIWindow?\n" +
                "    \n" +
                "    // Variable to keep the MMDrawerController instance\n" +
                "    var centerContainer : MMDrawerController?\n" +
                "    \n" +
                "    //    var meteorClient = initialiseMeteor(\"pre2\", \"ws://localhost:4000/websocket\");\n" +
                "    var meteorClient = initialiseMeteor(\"pre2\", \"https://demo.rocket.chat/websocket\");" +
                "/* this is also a comment,\n" +
                " but written over multiple lines */\n" +
                "\n" +
                "\n" +
                "/* this is the start of the first multiline comment\n" +
                " /* this is the second, nested multiline comment */\n" +
                " this is the end of the first multiline comment */");

        sourceFileResults.setSourceFile(sourceFile);

        SwiftProcessor swiftProcessor = new SwiftProcessor(sourceFileResults);
        swiftProcessor.process();
    }

    @Test
    public void shouldContainSingleLineComments() {
        @SuppressWarnings("unchecked") List<String> comments = (List<String>) sourceFileResults.getProperties()
                .get(GitMinerConstants.PK_COMMON_COMMENTS);

        Assert.assertEquals(6, comments.size());
    }

}
