package at.fhj.antesk.gitminer.app.dto;

import org.junit.Assert;
import org.junit.Test;

public class ModifierCountTest {

    @Test
    public void shouldCalculateModifierCount() {
        ModifierCountSourceFile sourceFile = new ModifierCountSourceFile();
        sourceFile.setPublicCount(7);
        sourceFile.setPrivateCount(3);
        sourceFile.setProtectedCount(2);
        sourceFile.setPackagePrivateCount(1);

        Assert.assertEquals(0.54, sourceFile.getPublicFactor(), 0.01);
    }

}
