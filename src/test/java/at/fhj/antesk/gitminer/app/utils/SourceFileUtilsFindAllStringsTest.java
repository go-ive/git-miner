package at.fhj.antesk.gitminer.app.utils;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SourceFileUtilsFindAllStringsTest {

    @Test
    public void shouldFindStringsInString() {
        String testString = "This \"is\" a \"String\" and an \"\" empty one.";

        List<String> allStrings = SourceFileUtils.findAllStrings(testString);

        assertEquals("Incorrect number of strings found", 3, allStrings.size());
        assertEquals("Incorrect string representation", "is", allStrings.get(0));
        assertEquals("Incorrect string representation", "String", allStrings.get(1));
        assertEquals("Incorrect string representation", "", allStrings.get(2));
    }

    @Test
    public void shouldReturnEmptyListWhenNoStringsFound() {
        String testString = "There are no strings here.";

        List<String> allStrings = SourceFileUtils.findAllStrings(testString);

        assertNotNull(allStrings);
        assertEquals("Incorrect size of list", 0, allStrings.size());
    }

    @Test
    public void shouldHandleNullParameter() {
        List<String> allStrings = SourceFileUtils.findAllStrings(null);

        assertNotNull(allStrings);
        assertEquals("Incorrect size of list", 0, allStrings.size());
    }

    @Test
    public void shouldHandleEmptyString() {
        List<String> allStrings = SourceFileUtils.findAllStrings("");

        assertNotNull(allStrings);
        assertEquals("Incorrect size of list", 0, allStrings.size());
    }

    @Test
    public void shouldHandleUnclosedString() {
        String testString = "This begins \"a string\" and \"ends without";

        List<String> allStrings = SourceFileUtils.findAllStrings(testString);

        assertEquals("Incorrect size of list", 1, allStrings.size());
        assertEquals("Incorrect string representation", "a string", allStrings.get(0));
    }

}
