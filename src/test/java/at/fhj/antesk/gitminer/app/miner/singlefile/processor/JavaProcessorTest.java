package at.fhj.antesk.gitminer.app.miner.singlefile.processor;

import at.fhj.antesk.gitminer.app.config.Config;
import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class JavaProcessorTest {

    private static final String TEST_FILE_PATH = "src/test/resources/javaprocessor/ApiClientImpl.java";

    private SourceFileResults sourceFileResults = new SourceFileResults();

    @Before
    public void setUp() throws IOException {
        File testFile = new File(TEST_FILE_PATH);
        String repositoryPath = testFile.getAbsolutePath().replaceAll("\\\\", "/");
        repositoryPath = repositoryPath.substring(0, repositoryPath.indexOf(TEST_FILE_PATH));
        Config.getInstance().setProperty(Config.KEYS.CLI_PARAM_REPOSITORY_PATH, repositoryPath);

        JavaProcessor javaProcessor = new JavaProcessor(sourceFileResults);

        SourceFile sourceFile = new SourceFile();
        sourceFile.setName(TEST_FILE_PATH);
        sourceFile.setPathOnSystem(repositoryPath + TEST_FILE_PATH);

        File file = new File(repositoryPath + TEST_FILE_PATH);
        String fileContents = FileUtils.readFileToString(file);
        sourceFile.setFileContents(fileContents);
        sourceFileResults.setSourceFile(sourceFile);

        javaProcessor.process();
    }

    @Test
    public void shouldExtractPackageName() {
        assertEquals("Wrong package",
                "com.github.goive.steamapi.client",
                sourceFileResults.getProperties().get(GitMinerConstants.PK_JAVA_PACKAGES));
    }

}
