package at.fhj.antesk.gitminer.app.miner.singlefile.processor.xml;

import at.fhj.antesk.gitminer.app.config.GitMinerConstants;
import at.fhj.antesk.gitminer.app.dto.SourceFile;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AndroidManifestParserTest {

    private static final String TEST_FILE_PATH = "src/test/resources/xmlprocessor/AndroidManifest.xml";

    private AndroidManifestParser parser;

    private SourceFileResults sourceFileResults;
    private SourceFile sourceFile;

    @Before
    public void setUp() throws IOException {
        sourceFile = new SourceFile();
        sourceFile.setName(TEST_FILE_PATH);
        sourceFile.setFileContents(FileUtils.readFileToString(new File(TEST_FILE_PATH)));

        sourceFileResults = new SourceFileResults();
        sourceFileResults.setSourceFile(sourceFile);

        parser = new AndroidManifestParser(sourceFileResults);
    }

    @Test
    public void shouldExtractPermissionsUsed() {
        parser.parse();

        @SuppressWarnings("unchecked") List<String> usedPermissions = (List<String>) sourceFileResults.getProperties().get(GitMinerConstants.PK_XML_ANDROID_USED_PERMISSIONS);

        assertNotNull(usedPermissions);
        assertEquals("Size not correct", 8, usedPermissions.size());
        assertEquals("Third permission", "android.permission.WRITE_EXTERNAL_STORAGE", usedPermissions.get(2));
    }


}
