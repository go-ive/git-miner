package at.fhj.antesk.gitminer.app.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        FileUtilsExtractFileNameTest.class,
        FileUtilsExtractPathForFullPathTest.class,
        FileUtilsCountFilesPerPathTest.class,
        FileUtilsExtractFileExtension.class
})
public class FileUtilsTestSuite {

}
