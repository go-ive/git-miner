package at.fhj.antesk.gitminer.app.utils;

import org.junit.Assert;
import org.junit.Test;

public class SourceFileUtilsIsAbsoluteUrlTest {

    @Test
    public void shouldFindUrlInXmlTag() {
        String input = "<organizationUrl>http://github.com/go-ive</organizationUrl>";

        Assert.assertEquals("http://github.com/go-ive", SourceFileUtils.parseAbsoluteUrl(input));
    }

    @Test
    public void shouldFindUrlInRandomText() {
        String input = "asffgdfghttp://asdf.comdfsdfs";

        Assert.assertEquals("http://asdf.comdfsdfs", SourceFileUtils.parseAbsoluteUrl(input));
    }

    @Test
    public void shouldFindUrlInString() {
        String input = "\"http://test.com\"";

        Assert.assertEquals("http://test.com", SourceFileUtils.parseAbsoluteUrl(input));
    }

}
