package at.fhj.antesk.gitminer.app.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SourceFileUtilsFindAllStringsTest.class,
        SourceFileUtilsIsUrlTest.class,
        SourceFileUtilsIsAbsoluteUrlTest.class
})
public class SourceFileUtilsTestSuite {

}
