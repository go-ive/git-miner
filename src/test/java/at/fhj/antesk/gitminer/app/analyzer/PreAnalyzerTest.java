package at.fhj.antesk.gitminer.app.analyzer;

import at.fhj.antesk.gitminer.app.config.Config;
import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.app.dto.SourceFileResults;
import at.fhj.antesk.gitminer.app.gitapi.GitAPI;
import at.fhj.antesk.gitminer.app.gitapi.GitAPIJGitImpl;
import at.fhj.antesk.gitminer.persistence.Database;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class PreAnalyzerTest {

    private static final String REPO_FILE_PATH = "generated/workdir/";
    private static final File PATH = new File(REPO_FILE_PATH);
    private static final String REPO_URL = "https://github.com/go-ive/steam-api.git";
    private static final String REF_2_1 = "refs/remotes/origin/2.1";
    private static final String BRANCHES_PATH = "generated/tempbranches";
    private static final int DATABASE_CALLS = 265;

    private Database database;
    private PreAnalyzer preAnalyzer;

    @BeforeClass
    public static void setUp() throws IOException, GitAPIException {
        FileUtils.forceMkdir(new File(REPO_FILE_PATH));
        FileUtils.forceMkdir(new File(BRANCHES_PATH));

        FileUtils.cleanDirectory(PATH);
        FileUtils.cleanDirectory(new File(BRANCHES_PATH));

        Git.cloneRepository()
                .setBare(false)
                .setCloneAllBranches(true)
                .setDirectory(PATH).setURI(REPO_URL)
                .call();
    }

    @Before
    public void setup() throws IOException {
        Config.getInstance().setProperty(Config.KEYS.ANALYSIS_MAX_THREADS, "1");
        Config.getInstance().setProperty(Config.KEYS.GM_HOME_PATH, BRANCHES_PATH);

        database = mock(Database.class);
        GitAPI gitAPI = new GitAPIJGitImpl(REPO_FILE_PATH);

        RepositoryDTO repositoryDto = new RepositoryDTO("name", "url", REPO_FILE_PATH, REF_2_1);
        preAnalyzer = new PreAnalyzer(repositoryDto, database, gitAPI);
    }

    @Test
    public void shouldAnalyzeSteamApiWith1Thread() {
        preAnalyzer.analyze();

        verify(database, times(DATABASE_CALLS)).insertSourceFileResults(any(SourceFileResults.class), anyInt());
    }

    @Test
    public void shouldAnalyzeSteamApiWith4Threads() {
        Config.getInstance().setProperty(Config.KEYS.ANALYSIS_MAX_THREADS, "4");

        preAnalyzer.analyze();

        verify(database, times(DATABASE_CALLS)).insertSourceFileResults(any(SourceFileResults.class), anyInt());
    }

}
