package at.fhj.antesk.gitminer.app.utils;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SourceFileUtilsIsUrlTest {

    @Test
    public void shouldReturnFalseForEmptyUrl() {
        assertFalse(SourceFileUtils.isUrl(""));
    }

    @Test
    public void shouldReturnFalseForNullParameter() {
        assertFalse(SourceFileUtils.isUrl(null));
    }

    @Test
    public void shouldReturnFalseForPossibleUrlWithSpaces() {
        assertFalse(SourceFileUtils.isUrl("http://a space"));
    }

    @Test
    public void shouldReturnTrueForPossibleUrlContainingSchema() {
        assertTrue(SourceFileUtils.isUrl("http://www.google.com"));
        assertTrue(SourceFileUtils.isUrl("ftp://filezilla.org"));
        assertTrue(SourceFileUtils.isUrl("gopher://government.gov"));
    }

    @Test
    public void shouldReturnTrueForPossibleUrlStartingWithDoubleSlash() {
        assertTrue(SourceFileUtils.isUrl("//this"));
        assertTrue(SourceFileUtils.isUrl("//this/url/is/ok"));
    }

    @Test
    public void shouldReturnTrueForPartialUrlWithoutSchema() {
        assertTrue(SourceFileUtils.isUrl("www.google.com"));
    }

    @Test
    public void shouldReturnTrueForPartialUrlPath() {
        assertTrue(SourceFileUtils.isUrl("api/v1"));
        assertTrue(SourceFileUtils.isUrl("api/v1/testendpoint"));
    }

}
