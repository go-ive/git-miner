package at.fhj.antesk.gitminer.app.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ive on 30.10.15.
 */
public class FileUtilsExtractFileNameTest {

    @Test
    public void shouldReturnFileNameWithExtension() {
        String fileName = FileUtils.extractFileName("./my/test/path/myfile.txt");

        assertEquals("File name is incorrect", "myfile.txt", fileName);
    }

    @Test
    public void shouldReturnFileNameWithoutExtension() {
        String fileName = FileUtils.extractFileName("./my/test/path/myfile");

        assertEquals("File name is incorrect", "myfile", fileName);
    }

    @Test
    public void shouldReturnFileNameIfNoPath() {
        String fileName = FileUtils.extractFileName("myfile");

        assertEquals("File name is incorrect", "myfile", fileName);
    }

    @Test
    public void shouldReturnFileNameWithDot() {
        String fileName = FileUtils.extractFileName(".myfile");

        assertEquals("File name is incorrect", ".myfile", fileName);
    }

    @Test
    public void shouldReturnFileNameWithDotInPath() {
        String fileName = FileUtils.extractFileName("/something/.myfile");

        assertEquals("File name is incorrect", ".myfile", fileName);
    }

}
