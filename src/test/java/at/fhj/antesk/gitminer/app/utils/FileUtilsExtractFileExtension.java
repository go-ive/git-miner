package at.fhj.antesk.gitminer.app.utils;

import org.junit.Assert;
import org.junit.Test;

public class FileUtilsExtractFileExtension {

    @Test
    public void shouldReturnExtensionForAbsolutePath() {
        String extension = FileUtils.extractFileExtension("/a/b.txt");

        Assert.assertEquals("Extension not correct", "txt", extension);
    }

    @Test
    public void shouldReturnExtensionForRelativePath() {
        String extension = FileUtils.extractFileExtension("a/b.txt");

        Assert.assertEquals("Extension not correct", "txt", extension);
    }

    @Test
    public void shouldReturnExtensionForNoPath() {
        String extension = FileUtils.extractFileExtension("b.txt");

        Assert.assertEquals("Extension not correct", "txt", extension);
    }

    @Test
    public void shouldReturnEmptyExtension() {
        String extension = FileUtils.extractFileExtension("b");

        Assert.assertEquals("Extension not correct", "", extension);
    }

    @Test
    public void shouldReturnEmptyExtensionForPath() {
        String extension = FileUtils.extractFileExtension("/a/b");

        Assert.assertEquals("Extension not correct", "", extension);
    }

}
