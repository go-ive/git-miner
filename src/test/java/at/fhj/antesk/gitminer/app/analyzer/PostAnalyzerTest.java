package at.fhj.antesk.gitminer.app.analyzer;

import at.fhj.antesk.gitminer.app.dto.RepositoryDTO;
import at.fhj.antesk.gitminer.persistence.Database;
import org.junit.Ignore;
import org.junit.Test;

import static org.mockito.Mockito.mock;

@Ignore
public class PostAnalyzerTest {

    @Test
    public void shouldRunAnalysis() {
        RepositoryDTO repositoryDto = new RepositoryDTO("name", "url", "path", "ref");
        Database database = mock(Database.class);
        PostAnalyzer postAnalyzer = new PostAnalyzer(repositoryDto, database);

        postAnalyzer.analyze();
    }

}
