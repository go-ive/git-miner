-------------------------------------------------------------------------------
-- Select files that contain public modifiers ordered by highest (slow)
-------------------------------------------------------------------------------

select file_path
     , data->>'publicModifierCount' as public_modifier_count
     , revision
  from source_file_results
 where repository_id = 1
   and version = 1
   and (data->'publicModifierCount') is not null
   and cast(data->>'publicModifierCount' as int) > 0
 order by cast(data->>'publicModifierCount' as int) desc;


-------------------------------------------------------------------------------
-- Fix \u0000 unicode issue in json
-------------------------------------------------------------------------------

update source_file_results
   set data = regexp_replace(data::text, '\\u0000', '', 'g')::json
 where id in (select id
                from source_file_results
               where data::text like '%\\u0000%');

-------------------------------------------------------------------------------
-- Select files with most changes
-------------------------------------------------------------------------------

select file_path
     , count(revision) as changes
  from source_file_results
 where repository_id = 1
   and version = 1
 group by file_path
having count(revision) >= 50
 order by changes desc;

-------------------------------------------------------------------------------
-- Get all changes for a file (not ordered)
-------------------------------------------------------------------------------

select revision
  from source_file_results
 where file_path = 'pom.xml';

-------------------------------------------------------------------------------
-- Get all found email addresses
-------------------------------------------------------------------------------

select data->>'emailAddresses'
  from source_file_results
 where (data->'emailAddresses') is not null
   and repository_id = 20
  ;

-------------------------------------------------------------------------------
-- Get all comments per file and revision
-------------------------------------------------------------------------------

select data#>'{comments}'
     , file_path
     , revision
  from source_file_results
 where (data->'comments') is not null
   and repository_id = 20
 order by file_path, revision asc
  ;