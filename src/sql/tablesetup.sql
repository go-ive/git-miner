﻿DROP TABLE IF EXISTS source_file_results;
DROP TABLE IF EXISTS repository;

CREATE TABLE repository (
	id SERIAL,
	name VARCHAR(100) NOT NULL,
	url VARCHAR(1000) NULL,
	directory VARCHAR(5000) NOT NULL,
	ref VARCHAR(500) NOT NULL,
	CONSTRAINT pk_repository_id PRIMARY KEY (id)
);

CREATE TABLE source_file_results (
    id SERIAL,
    repository_id INT REFERENCES repository(id),
    file_path VARCHAR(5000) NOT NULL,
    revision VARCHAR(40) NOT NULL,
    data json NOT NULL,
    version INT NOT NULL,
    CONSTRAINT pk_source_file_results_id PRIMARY KEY (id),
    UNIQUE (repository_id, file_path, revision, version)
);
