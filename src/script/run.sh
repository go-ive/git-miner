#!/bin/bash

readonly TEMP_DIR="repository"
readonly GIT_MINER_BIN="./git-miner"
readonly ARGS="$@"

clone_repository() {
    echo "Cloning $1 into ${TEMP_DIR}"

    git clone $1 ${TEMP_DIR}
}

process_repository() {
    echo "Processing..."

    ${GIT_MINER_BIN} -path ${TEMP_DIR} -o runPreAnalysis -url $1
}

delete_repository() {
    echo "Deleting repository $1"

    rm -rf ${TEMP_DIR}
}

main() {
    while read url
    do
        clone_repository $url
        process_repository $url
        delete_repository $url
    done < ${ARGS[0]}
}

main