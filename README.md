# Git Miner

## Master Thesis 2015/16

Git Miner is a Java-based application that can iterate through all revisions of 
a git repository and apply definable processing steps on files in every revision. 
This makes it possible to thoroughly analyze the whole content of a repository
for all kinds of metrics. The processors can be defined by implementing an interface
with custom implementations for all possible file types.

## Requirements

* JDK 1.8_91 (64 bit)
* PostgreSQL 9.4 (Optional)

## Build

    ./gradlew clean build

## Execute

1. Within {project.src}/build/distribution extract the gitminer-{version}.zip
2. Edit the bin/config.properties file and adapt the values if necessary
3. Run the tablesetup.sql script in the database (optional)
4. Run bin/git-miner and check the usage

## Example usage

    ./git-miner -path /path/to/git/repository -testMode
    
Test mode does not require a PostgreSQL database, but removes all fine-grained
data before each execution.

## Extend

To create processors for new file types:

1. Create a new class in the at.fhj.antesk.gitminer.app.miner.singlefile.processor package with the suffix "Processor".
2. Name it according to the file extension (e.g. FtlProcessor for .ftl files).
2. Extend AbstractProcessor.
3. Implement the process() method.
4. Don't forget to call super.process()!