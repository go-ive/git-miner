README
======

GitMiner is a client/server application that can clone git repositories and analyze them according to certain criteria.
The output is displayed in a Web UI and can be adapted and extended indefinitely.

Author: Ivan Antes-Klobucar
E-Mail: ivan.antes-klobucar@edu.fh-joanneum.at
